<?php

class User extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */
	public function index()
	{
		// $query = $this->db->query('SELECT * FROM t_user');
        $this->db->select('*');
        $this->db->from('t_user');
        $query = $this->db->get();
        $data['user'] =  $query;
        // $row = $query->row_array();
        // $this->load->view('news/content/index',$data);
		$this->template->load('news/template/layout','news/data_post/index',$data);
	}
}