<!-- Styles -->
<style>
    #chartdiv {
        width: 100%;
        height: 500px;
        max-width: 100%;
    }

    #chart_bar {
        width: 100%;
        height: 300px;
        max-width: 100%;
    }

    #chart_radar {
        width: 100%;
        height: 400px;
        font-size: 8pt
    }

    #chart_line_vote {
        width: 100%;
        height: 300px;
        max-width: 100%;
    }

    #chart_line_eotm {
        width: 100%;
        height: 300px;
        max-width: 100%;
    }

    #chart_bar_skp {
        width: 100%;
        height: 300px;
        max-width: 100%;
    }

    #chart_bar_absensi {
        width: 100%;
        height: 300px;
        max-width: 100%;
    }
</style>

<!-- Resources -->
<script src="https://cdn.amcharts.com/lib/5/index.js"></script>
<script src="https://cdn.amcharts.com/lib/5/xy.js"></script>
<script src="https://cdn.amcharts.com/lib/5/radar.js"></script>
<script src="https://cdn.amcharts.com/lib/5/themes/Animated.js"></script>

<!-- Chart code -->
<script>
    var root;
    var root_bar;
    var root_radar;
    var root_vote;
    var root_eotm;
    var root_skp;
    var root_absensi;
    var chart;
    var chart_bar;
    var chart_radar;
    var series;
    var defaultAxis = 100;
    var xAxis;
    var yAxis;
    var Status = "Pemetaan";
    var arr_kompetensi = [];
    var test_dinamic_data = [];
    var dinamic_data = [];
    var default_data = [];
    var all_data = [];
    var tabelPegawai;
    var tabelSumbuX;
    var tabelSumbuY;
    var tabelDiklat;
    var tabelPendidikan;
    var tabelBahasa;
    var total_data_series = 0;
    var coba = [];
    var grouping = [];
    var datatable = [];
    var axes_series = []
    var data_by_box = []
    var box_selected = 0;
    //initiate dataa
    var firstData = 0;
    var lastData = 3;
    var current_Data = firstData;
    var _ax = 33.33;
    var _ay = 50;
    var _bx = 66.67;
    var _by = 75;
    var _cx = 100;
    var _cy = 100;



    var data = [];
    var n_data = 0;
    var circleTemplate;
    var circleTemplate2;

    //variable untuk chart radar
    var cursor_radar;
    var xRenderer_radar;
    var series1_radar;
    var series2_radar;
    var xAxis_radar;
    var yAxis_radar;


    function push_datatable(list) {
        //push on datatable
        $.each(list, function(key, value) {
            var nilai_x = (Math.round(value.x * 100) / 100).toFixed(2)
            var nilai_y = (Math.round(value.y * 100) / 100).toFixed(2)
            var categori_box = cek_box_categori(value);
            var data = {
                "DT_RowId": value.data.nip,
                "name": value.data.nama,
                "x": nilai_x,
                "y": nilai_y,
                "categori": categori_box,
            }
            datatable.push(data);
        })
    }

    function show_tabel_by_jabatan(param) {
        var employes = []
        var data_list = []
        var i = 0;
        // kosongin tabel
        // console.log(box_selected)
        // console.log(axes_series)
        // console.log(axes_series[box_selected])
        datatable = []
        tabelPegawai.clear().draw()
        if (box_selected == 0) {
            $.each(test_dinamic_data, function(key, value) {
                employes[key] = value;
                // filter yg masuk dalam series
                data_list[i] = employes[key].filter(function(employe) {

                    result = employe.data.kelompok_jabatan_detail == param;
                    return result;
                });
                push_datatable(data_list[i])
                i++;
            })
        } else {
            $.each(test_dinamic_data, function(key, value) {
                employes[key] = value;
                // filter yg masuk dalam series
                data_list[i] = employes[key].filter(function(employe) {
                    result = employe.x <= axes_series[box_selected - 1].x_max && employe.x >= axes_series[
                            box_selected - 1]
                        .x_min && employe
                        .y <= axes_series[box_selected - 1].y_max && employe.y >= axes_series[box_selected -
                            1].y_min && employe.data.kelompok_jabatan_detail == param;

                    return result;
                });
                push_datatable(data_list[i])
                i++;
            })
        }
        //add data to table
        tabelPegawai.rows.add(datatable).draw();

        $('html, body').animate({
            scrollTop: $("#all_data").offset().top
        }, 100);
    }

    function show_tabel_by_box(param) {
        box_selected = param + 1
        clicked_series(axes_series[param])
        $('html, body').animate({
            scrollTop: $("#all_data").offset().top
        }, 100);
    }

    function find_pegawai(param) {
        var employes = []
        var data_list = []
        var i = 0;
        // kosongin tabel
        // console.log(box_selected)
        // console.log(axes_series)
        // console.log(axes_series[box_selected])
        datatable = []
        tabelPegawai.clear().draw()
        $.each(test_dinamic_data, function(key, value) {
            employes[key] = value;
            // filter yg masuk dalam series
            data_list[i] = employes[key].filter(function(employe) {
                result = employe.data.nama.toLowerCase().indexOf(param.toLowerCase()) >= 0
                // result = employe.data.kelompok_jabatan_detail == param;
                return result;
            });
            push_datatable(data_list[i])
            i++;
        })
        //add data to table
        tabelPegawai.rows.add(datatable).draw();

        $('html, body').animate({
            scrollTop: $("#all_data").offset().top
        }, 100);
    }


    function clicked_series(axes_series_n) {
        tabelPegawai.clear().draw();
        var data_list = []
        var i = 0;
        var employes = []
        $('#tabel-list-by-jabatan tbody').empty();
        datatable = []
        total_data_series = 0;
        remove_box_data()
        $.each(test_dinamic_data, function(key, value) {
            employes[key] = value;
            // filter yg masuk dalam series
            data_list[i] = employes[key].filter(function(employe) {
                result = employe.x <= axes_series_n.x_max && employe.x >= axes_series_n
                    .x_min && employe
                    .y <= axes_series_n.y_max && employe.y >= axes_series_n.y_min;
                return result;
            });
            view_on_list(data_list[i])

        })

        tabelPegawai.rows.add(datatable).draw();
        tabel_list = `<tr><td><b>Total</b></td><td class="text-center"><b>` + total_data_series +
            `</b></td></tr>`
        $('#tabel-list-by-jabatan tbody').append(tabel_list);

        // hidden radar
        $('#hasil_uji_kompetensi').hide();
        //munculin judul pada tabel 
        $('#judul_tabel').text("Series " + box_selected);

        // show filter box yg dipilih

        // $('#tabel-list-by-box tboddy tr').hide()
        $('#tabel-list-by-box').find('tbody tr').hide();
        $('#total_box').parent().parent().show()
        if (box_selected == 1) {
            $('#box1').parent().show()
        } else if (box_selected == 2) {
            $('#box2').parent().show()
        } else if (box_selected == 3) {
            $('#box3').parent().show()
        } else if (box_selected == 4) {
            $('#box4').parent().show()
        } else if (box_selected == 5) {
            $('#box5').parent().show()
        } else if (box_selected == 6) {
            $('#box6').parent().show()
        } else if (box_selected == 7) {
            $('#box7').parent().show()
        } else if (box_selected == 8) {
            $('#box8').parent().show()
        } else if (box_selected == 9) {
            $('#box9').parent().show()
        } else {
            $('#tabel-list-by-box').find('tbody tr').show();
        }

    }

    function remove_box_data() {

        data_by_box.box1 = []
        data_by_box.box2 = []
        data_by_box.box3 = []
        data_by_box.box4 = []
        data_by_box.box5 = []
        data_by_box.box6 = []
        data_by_box.box7 = []
        data_by_box.box8 = []
        data_by_box.box9 = []
    }


    function cek_box_categori(value) {
        //series 1
        if (value.x < _ax && value.y < _ay) {
            return "Series 1";
        }
        // series 2
        else if (_ax < value.x && value.x < _bx && value.y < _ay) {
            return "Series 3";
        }
        // series 3
        else if (value.x < _ax && _ay < value.y && value.y < _by) {
            return "Series 2";
        }
        // series 4
        else if (_bx < value.x && value.x < _cx && value.y < _ay) {
            return "Series 6";
        }
        // series 5
        else if (_ax < value.x && value.x < _bx && _ay < value.y && value.y < _by) {
            return "Series 5";
        }
        // series 6
        else if (value.x < _ax && _by < value.y && value.y < _cy) {
            return "Series 6";
        }
        // series 7
        else if (_bx < value.x && value.x < _cx && _ay < value.y && value.y < _by) {
            return "Series 8";
        }
        // series 8
        else if (_ax < value.x && value.x < _bx && _by < value.y && value.y < _cy) {
            return "Series 7";
        }
        // series 9
        else if (_bx < value.x && value.x < _cx && _by < value.y && value.y < _cy) {
            return "Series 9";
        } else {
            return "Series x";
        }
    }

    function view_on_list(list) {
        html = "";

        // $('#list-detail-data').show();
        //prepare untuk tabel dibawah
        $('#personal_detail').hide();
        $.each(list, function(key, value) {
            var nilai_x = (Math.round(value.x * 100) / 100).toFixed(2)
            var nilai_y = (Math.round(value.y * 100) / 100).toFixed(2)
            var categori_box = cek_box_categori(value);
            var data = {
                "DT_RowId": value.data.nip,
                "name": value.data.nama,
                "x": nilai_x,
                "y": nilai_y,
                "categori": categori_box,
            }
            datatable.push(data);
        });
        // $('#table-detail tbody').append(html)
        // $('html, body').animate({
        //     scrollTop: $("#table-detail").offset().top
        // }, 100);
        //add to list kanan
        if (list.length > 0) {
            tabel_list = `<tr onclick="show_tabel_by_jabatan('` + list[0].data
                .kelompok_jabatan_detail + `')"><td>` + list[0].data
                .kelompok_jabatan_detail + `</td><td class="text-center" >` + list.length +
                `</td></tr>`
            $('#tabel-list-by-jabatan tbody').append(tabel_list);
            total_data_series = total_data_series + list.length;
        }
        filter_by_box(list)
    }

    //fungsi untuk filter berdasarkan boxnya,
    function filter_by_box(list) {

        if (list.length > 0) {
            $.each(list, function(key, value) {
                //series 1
                if (value.x < _ax && value.y < _ay) {
                    data_by_box.box1.push(value)
                }
                // series 3
                else if (_ax < value.x && value.x < _bx && value.y < _ay) {
                    data_by_box.box3.push(value)
                }
                // series 2
                else if (value.x < _ax && _ay < value.y && value.y < _by) {
                    data_by_box.box2.push(value)
                }
                // series 6
                else if (_bx < value.x && value.x < _cx && value.y < _ay) {
                    data_by_box.box6.push(value)
                }
                // series 5
                else if (_ax < value.x && value.x < _bx && _ay < value.y && value.y < _by) {
                    data_by_box.box5.push(value)
                }
                // series 4
                else if (value.x < _ax && _by < value.y && value.y < _cy) {
                    data_by_box.box5.push(value)
                }
                // series 8
                else if (_bx < value.x && value.x < _cx && _ay < value.y && value.y < _by) {
                    data_by_box.box8.push(value)
                }
                // series 7
                else if (_ax < value.x && value.x < _bx && _by < value.y && value.y < _cy) {
                    data_by_box.box7.push(value)

                }
                // series 9
                else if (_bx < value.x && value.x < _cx && _by < value.y && value.y < _cy) {
                    data_by_box.box9.push(value)
                }

            });

        }

        $('#box1').text(data_by_box.box1.length)
        presentase = (Math.round((data_by_box.box1.length / total_data_series * 100) * 100) / 100).toFixed(2)
        presentase = (presentase != 'NaN') ? presentase : (0.00).toFixed(2);
        $('#box1').next().text(presentase + ' %')
        $('#box2').text(data_by_box.box2.length)
        presentase = (Math.round((data_by_box.box2.length / total_data_series * 100) * 100) / 100).toFixed(2)
        presentase = (presentase != 'NaN') ? presentase : (0.00).toFixed(2);
        $('#box2').next().text(presentase + ' %')
        $('#box3').text(data_by_box.box3.length)
        presentase = (Math.round((data_by_box.box3.length / total_data_series * 100) * 100) / 100).toFixed(2)
        presentase = (presentase != 'NaN') ? presentase : (0.00).toFixed(2);
        $('#box3').next().text(presentase + ' %')
        $('#box4').text(data_by_box.box4.length)
        presentase = (Math.round((data_by_box.box4.length / total_data_series * 100) * 100) / 100).toFixed(2)
        presentase = (presentase != 'NaN') ? presentase : (0.00).toFixed(2);
        $('#box4').next().text(presentase + ' %')
        $('#box5').text(data_by_box.box5.length)
        presentase = (Math.round((data_by_box.box5.length / total_data_series * 100) * 100) / 100).toFixed(2)
        presentase = (presentase != 'NaN') ? presentase : (0.00).toFixed(2);
        $('#box5').next().text(presentase + ' %')
        $('#box6').text(data_by_box.box6.length)
        presentase = (Math.round((data_by_box.box6.length / total_data_series * 100) * 100) / 100).toFixed(2)
        presentase = (presentase != 'NaN') ? presentase : (0.00).toFixed(2);
        $('#box6').next().text(presentase + ' %')
        $('#box7').text(data_by_box.box7.length)
        presentase = (Math.round((data_by_box.box7.length / total_data_series * 100) * 100) / 100).toFixed(2)
        presentase = (presentase != 'NaN') ? presentase : (0.00).toFixed(2);
        $('#box7').next().text(presentase + ' %')
        $('#box8').text(data_by_box.box8.length)
        presentase = (Math.round((data_by_box.box8.length / total_data_series * 100) * 100) / 100).toFixed(2)
        presentase = (presentase != 'NaN') ? presentase : (0.00).toFixed(2);
        $('#box8').next().text(presentase + ' %')
        $('#box9').text(data_by_box.box9.length)
        presentase = (Math.round((data_by_box.box9.length / total_data_series * 100) * 100) / 100).toFixed(2)
        presentase = (presentase != 'NaN') ? presentase : (0.00).toFixed(2);
        $('#box9').next().text(presentase + ' %')
        $('#total_box').text(total_data_series)
        presentase = (Math.round((total_data_series / total_data_series * 100) * 100) / 100).toFixed(2)
        presentase = (presentase != 'NaN') ? presentase : (0.00).toFixed(2);
        $('#total_box').parent().next().text(presentase + ' %')
    }


    function removeSeries(chart) {
        length_series = chart.series.length;
        for (var i = 0; i < length_series; i++) {
            chart.series.removeIndex(0);
        }

    }


    function initiate_chart() {
        // https://www.amcharts.com/docs/v5/getting-started/#Root_element
        root = am5.Root.new("chartdiv");

        // Set themes
        // https://www.amcharts.com/docs/v5/concepts/themes/
        root.setThemes([
            am5themes_Animated.new(root)
        ]);
    }

    function generate_chart(data_bullet) {
        // Create root element



        root.container.children.clear();
        // Create chart
        // https://www.amcharts.com/docs/v5/charts/xy-chart/
        chart = root.container.children.push(am5xy.XYChart.new(root, {
            panX: true,
            panY: true,
            wheelY: "zoomXY",
            pinchZoomX: true,
            pinchZoomY: true
        }));

        // Create axes
        // https://www.amcharts.com/docs/v5/charts/xy-chart/axes/
        xAxis = chart.xAxes.push(am5xy.ValueAxis.new(root, {
            min: 0,
            max: defaultAxis,
            visible: true,
            renderer: am5xy.AxisRendererX.new(root, {
                minGridDistance: 50,
            }),
            tooltip: am5.Tooltip.new(root, {})
        }));




        yAxis = chart.yAxes.push(am5xy.ValueAxis.new(root, {
            min: 0,
            max: defaultAxis,
            visible: true,
            renderer: am5xy.AxisRendererY.new(root, {}),
            tooltip: am5.Tooltip.new(root, {})
        }));




        // move grid forward
        chart.plotContainer.children.moveValue(
            chart.gridContainer,
            chart.plotContainer.children.length - 1
        );



        // Add cursor
        // https://www.amcharts.com/docs/v5/charts/xy-chart/cursor/
        chart.set("cursor", am5xy.XYCursor.new(root, {
            xAxis: xAxis,
            yAxis: yAxis
        }));

        // Add scrollbars
        // https://www.amcharts.com/docs/v5/charts/xy-chart/scrollbars/
        chart.set("scrollbarX", am5.Scrollbar.new(root, {
            orientation: "horizontal"
        }));

        chart.set("scrollbarY", am5.Scrollbar.new(root, {
            orientation: "vertical"
        }));

        // Label
        // var label = chart.plotContainer.children.push(am5.Label.new(root, {
        //     text: "Bulan " + (current_Data + 1).toString(),
        //     fontSize: "3em",
        //     fill: am5.color(0x000000),
        //     opacity: 0.3
        // }));

        // Create controls
        var container = chart.plotContainer.children.push(am5.Container.new(root, {
            y: am5.p100,
            centerX: am5.p50,
            centerY: am5.p100,
            x: am5.p50,
            width: am5.percent(90),
            layout: root.horizontalLayout,
            paddingBottom: 10
        }));


        //INITIATE UNTUK GEERATE SERIES SESUAI DEFAULT AXIS
        // generate_series(ax,ay, bx,by,cx,cy);
        generate_series(_ax, _ay, _bx, _by, _cx, _cy)

        //SLIDER UNTUK VIEW DATA PER BULAN
        // var slider = container.children.push(am5.Slider.new(root, {
        //     orientation: "horizontal",
        //     start: 0,
        //     centerY: am5.p50
        // }));

        // slider.on("start", function(start) {
        //     if (start === 1) {
        //         // playButton.set("active", false);
        //     }
        // });

        // slider.events.on("rangechanged", function() {
        //     updateSeriesData(
        //         firstData + Math.round(slider.get("start", 0) * (lastData - firstData))
        //     );
        // });

        //END UNTUK SLIDER


        //UNTUK UPDATE PERBULAN
        // function updateSeriesData(_n_data) {
        //     if (current_Data != _n_data) {
        //         current_Data = _n_data;

        //         //data 1
        //         var data = data_pegawai[_n_data];

        //         var i = 0;
        //         am5.array.each(data, function(item) {
        //             series.data.setIndex(i, item);
        //             i++;
        //         });

        //         // data 2
        //         var data2 = data_pegawai2[_n_data];

        //         var i = 0;
        //         am5.array.each(data2, function(item) {
        //             series_data2.data.setIndex(i, item);
        //             i++;
        //         });


        //         n = _n_data + 1;
        //         label.set("text", "Data " + n.toString());
        //     }
        // }


        // Make stuff animate on load
        // https://www.amcharts.com/docs/v5/concepts/animations/
        chart.appear(1000, 100);





        //fungsi tambahan untuk management  series
        //generate series
        //generate sesuai scaling yg di inginkan
        function generate_series(_ax, _ay, _bx, _by, _cx, _cy) {
            // Pewarnaan
            var series0 = chart.series.push(am5xy.LineSeries.new(root, {
                xAxis: xAxis,
                yAxis: yAxis,
                valueXField: "ax",
                valueYField: "ay",
                tooltipText: "Series 4",
                fill: am5.color("#a8e6cf"),
                // description: "coba" //tambahan
            }));

            series0.fills.template.setAll({
                fillOpacity: 0.9,
                visible: true
            });
            series0.strokes.template.set("forceHidden", true);

            series0.data.setAll([{
                    ax: 0,
                    ay: _by
                },
                {
                    ax: _ax,
                    ay: _by
                },
                {
                    ax: _ax,
                    ay: _cy
                },
                {
                    ax: 0,
                    ay: _cy
                },
            ]);
            //add detail an series
            series0.data.detail_series = {
                x_min: 0,
                x_max: _ax,
                y_min: _by,
                y_max: _cy,
            }

            var series1 = chart.series.push(am5xy.LineSeries.new(root, {
                xAxis: xAxis,
                yAxis: yAxis,
                valueXField: "ax",
                valueYField: "ay",
                tooltipText: "Series 2",
                fill: am5.color("#dcedc1")
            }));
            series1.fills.template.setAll({
                fillOpacity: 0.9,
                visible: true,
            });
            series1.strokes.template.set("forceHidden", true);
            series1.data.setAll([{
                    ax: 0,
                    ay: _by
                },
                {
                    ax: _ax,
                    ay: _by
                },
                {
                    ax: _ax,
                    ay: _ay
                },
                {
                    ax: 0,
                    ay: _ay
                }
            ]);
            //add detail an series
            series1.data.detail_series = {
                x_min: 0,
                x_max: _ax,
                y_min: _ay,
                y_max: _by,
            }

            var series2 = chart.series.push(am5xy.LineSeries.new(root, {
                xAxis: xAxis,
                yAxis: yAxis,
                valueXField: "ax",
                valueYField: "ay",
                tooltipText: "Series 1",
                fill: am5.color("#ffd3b6")
            }));
            series2.fills.template.setAll({
                fillOpacity: 1,
                visible: true
            });
            series2.strokes.template.set("forceHidden", true);
            series2.data.setAll([{
                    ax: 0,
                    ay: _ay
                },
                {
                    ax: _ax,
                    ay: _ay
                },
                {
                    ax: _ax,
                    ay: 0
                },
                {
                    ax: 0,
                    ay: 0
                }
            ]);
            //add detail an series
            series2.data.detail_series = {
                x_min: 0,
                x_max: _ax,
                y_min: 0,
                y_max: _ay,
            }

            var series3 = chart.series.push(am5xy.LineSeries.new(root, {
                xAxis: xAxis,
                yAxis: yAxis,
                valueXField: "ax",
                valueYField: "ay",
                tooltipText: "Series 3",
                fill: am5.color("#DEB6AB")
            }));
            series3.fills.template.setAll({
                fillOpacity: 0.9,
                visible: true
            });
            series3.strokes.template.set("forceHidden", true);
            series3.data.setAll([{
                    ax: _ax,
                    ay: 0
                },
                {
                    ax: _bx,
                    ay: 0
                },
                {
                    ax: _bx,
                    ay: _ay
                },
                {
                    ax: _ax,
                    ay: _ay
                }
            ]);

            //add detail an series
            series3.data.detail_series = {
                x_min: _ax,
                x_max: _bx,
                y_min: 0,
                y_max: _ay,
            }

            var series4 = chart.series.push(am5xy.LineSeries.new(root, {
                xAxis: xAxis,
                yAxis: yAxis,
                valueXField: "ax",
                valueYField: "ay",
                tooltipText: "Series 5",
                fill: am5.color("#ff8b94")
            }));
            series4.fills.template.setAll({
                fillOpacity: 0.9,
                visible: true
            });
            series4.strokes.template.set("forceHidden", true);
            series4.data.setAll([{
                    ax: _ax,
                    ay: _ay
                },
                {
                    ax: _bx,
                    ay: _ay
                },
                {
                    ax: _bx,
                    ay: _by
                },
                {
                    ax: _ax,
                    ay: _by
                }
            ]);
            series4.data.detail_series = {
                x_min: _ax,
                x_max: _bx,
                y_min: _ay,
                y_max: _by,
            }
            var series5 = chart.series.push(am5xy.LineSeries.new(root, {
                xAxis: xAxis,
                yAxis: yAxis,
                valueXField: "ax",
                valueYField: "ay",
                tooltipText: "Series 7",
                fill: am5.color("#F8ECD1")
            }));
            series5.fills.template.setAll({
                fillOpacity: 0.9,
                visible: true
            });
            series5.strokes.template.set("forceHidden", true);
            series5.data.setAll([{
                    ax: _ax,
                    ay: _by
                },
                {
                    ax: _bx,
                    ay: _by
                },
                {
                    ax: _bx,
                    ay: _cy
                },
                {
                    ax: _ax,
                    ay: _cy
                }
            ]);
            series5.data.detail_series = {
                x_min: _ax,
                x_max: _bx,
                y_min: _by,
                y_max: _cy,
            }
            var series6 = chart.series.push(am5xy.LineSeries.new(root, {
                xAxis: xAxis,
                yAxis: yAxis,
                valueXField: "ax",
                valueYField: "ay",
                tooltipText: "Series 9",
                fill: am5.color("#e6b89c")
            }));
            series6.fills.template.setAll({
                fillOpacity: 0.9,
                visible: true
            });
            series6.strokes.template.set("forceHidden", true);
            series6.data.setAll([{
                    ax: _bx,
                    ay: _by
                },
                {
                    ax: _cx,
                    ay: _by
                },
                {
                    ax: _cx,
                    ay: _cy
                },
                {
                    ax: _bx,
                    ay: _cy
                }
            ]);
            series6.data.detail_series = {
                x_min: _bx,
                x_max: _cx,
                y_min: _by,
                y_max: _cy,
            }


            var series7 = chart.series.push(am5xy.LineSeries.new(root, {
                xAxis: xAxis,
                yAxis: yAxis,
                valueXField: "ax",
                valueYField: "ay",
                tooltipText: "Series 8",
                fill: am5.color("#ead2ac")
            }));
            series7.fills.template.setAll({
                fillOpacity: 0.9,
                visible: true
            });
            series7.strokes.template.set("forceHidden", true);
            series7.data.setAll([{
                    ax: _bx,
                    ay: _by
                },
                {
                    ax: _cx,
                    ay: _by
                },
                {
                    ax: _cx,
                    ay: _ay
                },
                {
                    ax: _bx,
                    ay: _ay
                }
            ]);
            series7.data.detail_series = {
                x_min: _bx,
                x_max: _cx,
                y_min: _ay,
                y_max: _by,
            }

            var series8 = chart.series.push(am5xy.LineSeries.new(root, {
                xAxis: xAxis,
                yAxis: yAxis,
                valueXField: "ax",
                valueYField: "ay",
                tooltipText: "Series 6",
                fill: am5.color("#9cafb7")
            }));
            series8.fills.template.setAll({
                fillOpacity: 0.9,
                visible: true
            });
            series8.strokes.template.set("forceHidden", true);
            series8.data.setAll([{
                    ax: _bx,
                    ay: _ay
                },
                {
                    ax: _cx,
                    ay: _ay
                },
                {
                    ax: _cx,
                    ay: 0
                },
                {
                    ax: _bx,
                    ay: 0
                }
            ]);
            series8.data.detail_series = {
                x_min: _bx,
                x_max: _cx,
                y_min: 0,
                y_max: _ay,
            }



            //dinamic bullet
            dinamic_bullet(data_bullet)

            function dinamic_bullet(data_bullet) {
                // var series_arr = [];
                var circleTemplate = [];
                var employes = [];
                // var bulletCircle = [];

                var html = ''
                var i = 0;
                var randomColor = Math.floor(Math.random() * 16777215).toString(16)

                generate_button_filter2(test_dinamic_data)


                // $(".checkbox").change(function(){
                //     if ($('.checkbox:checked').length == $('.checkbox').length) {
                //         $('input:checkbox').attr('checked','checked');
                //     }
                // });
                $.each(data_bullet, function(key, value) {
                    var j = key;
                    var series_arr = chart.series.push(am5xy.LineSeries.new(root, {
                        calculateAggregates: true,
                        xAxis: xAxis,
                        yAxis: yAxis,
                        valueYField: "y",
                        valueXField: "x",
                        valueField: "value"
                    }));
                    // var colors = chart.get("colors");
                    // var warna = colors.next()
                    series_arr.strokes.template.set("visible", false);
                    // Add bullet Data 1
                    var key = am5.Template.new({});
                    series_arr.bullets.push(function() {
                        var bulletCircle = am5.Circle.new(root, {
                            radius: 10,
                            // fill: am5.color("#"+randomColor),
                            fill: series_arr.get('fill'),
                            stroke: am5.color(0xf3f3f3),
                            strokeWidth: 0.5,
                            // fill: warna ,
                            fillOpacity: 1,
                            tooltipText: "x: {valueX} y:{valueY} value: {value} data: {data.nama}"
                        }, key);
                        return am5.Bullet.new(root, {
                            sprite: bulletCircle
                        });
                    });
                    //add heat rule data 
                    series_arr.set("heatRules", [{
                        target: key,
                        min: 5,
                        max: 45,
                        dataField: "value",
                        key: "radius",
                        maxValue: 2000
                    }]);

                    series_arr.data.setAll(value);
                    series_arr.appear(1000);

                    key.events.on("click", function(ev) {
                        nip = ev.target.dataItem.dataContext.data.nip;
                        get_nilai_by_nip(nip);
                        // document.getElementById("nama_pegawai").innerHTML = ev.target.dataItem.dataContext.data.nama;
                    });

                    //function clicki
                    $('#category_' + i).change(function() {
                        if (this.checked) {
                            key.set("visible", true);
                            data_bullet[j] = value

                            // kosongin tabel
                            $('#tabel-list-by-jabatan tbody').empty();
                            $('#table-detail tbody').empty();
                            datatable = []
                            remove_box_data()
                            total_data_series = 0
                            tabelPegawai.clear().draw()
                            $.each(data_bullet, function(key, value) {
                                view_on_list(value)
                            })
                            //add data to table
                            tabelPegawai.rows.add(datatable).draw();
                            tabel_list = `<tr><td><b>Total</b></td><td class="text-center"><b>` +
                                total_data_series +
                                `</b></td></tr>`
                            $('#tabel-list-by-jabatan tbody').append(tabel_list);
                            // console.log(data_bullet)
                        } else {

                            key.set("visible", false);
                            data_bullet[j] = []
                            // kosongin tabel
                            $('#tabel-list-by-jabatan tbody').empty();
                            $('#table-detail tbody').empty();
                            datatable = []
                            remove_box_data()
                            total_data_series = 0
                            tabelPegawai.clear().draw()
                            $.each(data_bullet, function(key, value) {
                                view_on_list(value)
                            })
                            //add data to table
                            tabelPegawai.rows.add(datatable).draw();
                            tabel_list = `<tr><td><b>Total</b></td><td class="text-center"><b>` +
                                total_data_series +
                                `</b></td></tr>`
                            $('#tabel-list-by-jabatan tbody').append(tabel_list);

                            //jika uncheck hilangkan check pada select all

                            $('#select_all').prop('checked', false);
                        }

                    });
                    $('#select_all').change(function() {
                        if (this.checked) {
                            $('.filter-checkbox').prop('checked', true);
                            key.set("visible", true);
                        } else {
                            $('.filter-checkbox').prop('checked', false);
                            key.set("visible", false);
                        }
                    })
                    $('#show_all').on('click', function() {
                        $('#select_all').prop('checked', true);
                        $('.filter-checkbox').prop('checked', true);
                        key.set("visible", true);
                        box_selected = 0
                        $('#tabel-list-by-jabatan tbody').empty();
                        $('#table-detail tbody').empty();
                        datatable = []
                        remove_box_data()
                        total_data_series = 0
                        tabelPegawai.clear().draw()
                        //generate data on tabel
                        $.each(test_dinamic_data, function(key, value) {
                            view_on_list(value)
                        })
                        // console.log(datatable)
                        // console.log(datatable)
                        tabelPegawai.rows.add(datatable).draw();
                        //penambahan total
                        tabel_list = `<tr><td><b>Total</b></td><td class="text-center"><b>` +
                            total_data_series +
                            `</b></td></tr>`
                        $('#tabel-list-by-jabatan tbody').append(tabel_list);
                        $('#tabel-list-by-box').find('tbody tr').show();

                    });
                    $('#category_' + i).next().find('label').css("color", series_arr.get('fill'))
                    //set color to array

                    //set to data array for dinamic filter
                    // set_array_all[value[0].data.kelompok_jabatan_detail] = value
                    i++;
                });

                $(".filter-checkbox").change(function() {
                    if ($('.filter-checkbox:checked').length == $('.filter-checkbox').length) {
                        $('#select_all').prop('checked', true);
                    }
                });
                //click series
                //fungsi untuk ketika series di click
                axes_series[0] = series2.data.detail_series
                axes_series[1] = series1.data.detail_series
                axes_series[2] = series3.data.detail_series
                axes_series[3] = series0.data.detail_series
                axes_series[4] = series4.data.detail_series
                axes_series[5] = series8.data.detail_series
                axes_series[6] = series5.data.detail_series
                axes_series[7] = series7.data.detail_series
                axes_series[8] = series6.data.detail_series
                series0.events.on("click", function(ev) {
                    axes_series[3] = ev.target.data.detail_series
                    box_selected = 4;
                    clicked_series(axes_series[3])

                });
                series1.events.on("click", function(ev) {
                    axes_series[1] = ev.target.data.detail_series
                    box_selected = 2;
                    clicked_series(axes_series[1])

                });
                series2.events.on("click", function(ev) {
                    axes_series[0] = ev.target.data.detail_series
                    box_selected = 1;
                    clicked_series(axes_series[0])

                });
                series3.events.on("click", function(ev) {
                    axes_series[2] = ev.target.data.detail_series
                    box_selected = 3;
                    clicked_series(axes_series[2])

                });
                series4.events.on("click", function(ev) {
                    axes_series[4] = ev.target.data.detail_series
                    box_selected = 5;
                    clicked_series(axes_series[4])

                });
                series5.events.on("click", function(ev) {
                    axes_series[6] = ev.target.data.detail_series
                    box_selected = 7;
                    clicked_series(axes_series[6])

                });
                series6.events.on("click", function(ev) {
                    axes_series[8] = ev.target.data.detail_series
                    box_selected = 9;
                    clicked_series(axes_series[8])

                });
                series7.events.on("click", function(ev) {
                    axes_series[7] = ev.target.data.detail_series
                    box_selected = 8;
                    clicked_series(axes_series[7])

                });
                series8.events.on("click", function(ev) {
                    axes_series[5] = ev.target.data.detail_series
                    box_selected = 6;
                    clicked_series(axes_series[5])

                });

            }




        }

    }

    //FUNGSI UNTUK GENERATE BUTTON
    function generate_button_filter(list) {
        $('#filter_button').empty();
        $.each(list, function(key, value) {
            $('#filter_button').append('<button class="btn btn-outline-primary" id="btn-' + key + '">' + key +
                '</button>');
            //set series batas
            //add function cicked
            document.getElementById("btn-" + key).addEventListener("click", function() {

                _ax = value.ax;
                _ay = value.ay;
                _bx = value.bx;
                _by = value.by;
                _cx = value.cx;
                _cy = value.cy;
                removeSeries(chart)
                generate_chart(default_data)
                // generate_series(value.ax, value.ay, value.bx, value.by, value.cx, value.cy);
                $('#select_all').prop("checked", true);
            })


        });
    }

    function generate_button_filter2(data) {
        // console.log(data)
        $('#filter_category').empty();
        var jumlah = 0;
        var i = 0;

        $.each(data, function(key, value) {

            html = `<div class="pretty p-default">
                            <input type="checkbox" class='filter-checkbox' id='category_` + i + `' checked>
                            <div class="state p-success">
                                <label>` + value[0].data.kelompok_jabatan_detail + `</label>
                            </div>
                        </div>`;

            // console.log(list_color)
            $('#filter_category').append(html)

            //    console.log(list_color)
            i++;
            jumlah = jumlah + value.length;
        })



    }

    function fileter_data(_array, filter) {

        var response = [];
        $.each(_array, function(key, value) {
            person = {
                nip: value.nip,
                kd_jabatan: value.kd_jabatan,
                nama: value.nama,
                kelompok_jabatan: value.kelompok_jabatan,
                kelompok_jabatan_detail: value.kelompok_jabatan_detail,
                test: filter
            };

            if (value.kelompok_jabatan_detail == filter) {
                response.push({
                    x: value.xaxis100,
                    y: value.yaxis100,
                    value: 5,
                    data: person

                });
            }

        });

        return response;
    }
    //filter select by unit kerja
    function generate_filter_unit_kerja(list) {
        $('#filter-unit-kerja').empty();

        $('#filter-unit-kerja').append(`<option value="all">All</option>`);
        $.each(list, function(key, value) {
            $('#filter-unit-kerja').append(`<option value="${value.unit_eselon_ii}">
                                       ${value.unit_eselon_ii}
                                  </option>`);
        });
    }




    function groupBy(_array, key) {
        return _array.reduce(function(rv, x) {
            (rv[x[key]] = rv[x[key]] || []).push(x);
            return rv;
        }, {});
    }



    function get_nilai_by_nip(nip) {

        $('#hasil_uji_kompetensi').show();
        const api_url = "http://paspeg.kemendag.go.id/manajemen_talenta/neun_box/load/einzelheiten/" + nip;
        $.ajax({
            type: "GET", // Method pengiriman data bisa dengan GET atau POST
            url: api_url, // Isi dengan url/path file php yang dituju
            dataType: "json",
            success: function(response) {
                if (response) {
                    detail_nilai = response.kompetensi.nilai[0]
                    arr_kompetensi = response.kompetensi
                    //untuk chart bar
                    // removeSeries(chart_bar);

                    //chart radar
                    removeSeries(chart_radar);
                    generate_chart_radar(arr_kompetensi, Status)
                    document.getElementById("total").innerHTML = detail_nilai.total_nilai;

                    //set data detail pegawai
                    $('#nama').text(response.kepegawaian[0].nama);
                    $('#nama-eotm').text(response.kepegawaian[0].nama);
                    $('#pangkat').text(response.kepegawaian[0].pangkat);
                    $('#nip').text(response.kepegawaian[0].nip);
                    $('#nip-eotm').text(response.kepegawaian[0].nip);
                    $('#jabatan').text(response.kepegawaian[0].jabatan);
                    $('#unit_kerja').text(response.kepegawaian[0].unit_kerja);
                    $('#pendidikan').text(response.kepegawaian[0].pendidikan);
                    $('#img-profile').attr("src", response.photo.thumb);
                    $('#img-profile-2').attr("src", response.photo.thumb);
                    //detail nilai
                    $('#y_skp_15').text((Math.round(response.kompetensi.nilai_detail_box[0].y_skp_15 * 100) /
                        100).toFixed(2));
                    $('#y_vote_50').text((Math.round(response.kompetensi.nilai_detail_box[0].y_vote_50 * 100) /
                        100).toFixed(2));
                    $('#y_eotm_15').text((Math.round(response.kompetensi.nilai_detail_box[0].y_eotm_15 * 100) /
                        100).toFixed(2));
                    $('#y_absensi_5').text((Math.round(response.kompetensi.nilai_detail_box[0].y_absensi_5 *
                        100) / 100).toFixed(2));
                    $('#y_inovasi_15').text((Math.round(response.kompetensi.nilai_detail_box[0].y_inovasi_15 *
                        100) / 100).toFixed(2));
                    $('#x_kompetensi_45').text((Math.round(response.kompetensi.nilai_detail_box[0]
                        .x_kompetensi_45 * 100) / 100).toFixed(2));
                    $('#x_potensi_30').text((Math.round(response.kompetensi.nilai_detail_box[0].x_potensi_30 *
                        100) / 100).toFixed(2));
                    $('#x_diklat_3').text((Math.round(response.kompetensi.nilai_detail_box[0].x_diklat_3 *
                        100) / 100).toFixed(2));
                    $('#x_pendidikan_9').text((Math.round(response.kompetensi.nilai_detail_box[0]
                        .x_pendidikan_9 * 100) / 100).toFixed(2));
                    $('#x_penugasan_8').text((Math.round(response.kompetensi.nilai_detail_box[0]
                        .x_penugasan_8 * 100) / 100).toFixed(2));
                    $('#x_bahasa_5').text((Math.round(response.kompetensi.nilai_detail_box[0].x_bahasa_5 *
                        100) / 100).toFixed(2));
                    $('#xaxis100').text((Math.round(response.kompetensi.nilai_detail_box[0].xaxis100 * 100) /
                        100).toFixed(2));
                    $('#yaxis100').text((Math.round(response.kompetensi.nilai_detail_box[0].yaxis100 * 100) /
                        100).toFixed(2));


                    // nama pada grafik
                    $('#name_on_grap').text(response.kepegawaian[0].nama);
                    //hidden detail data and show personal data
                    // $('#list-detail-data').hide();
                    $('#personal_detail').show();



                    //set tabel
                    datatable = [];
                    tabelPegawai.clear().draw()
                    var cek = [];
                    cek.x = response.kompetensi.nilai_detail_box[0].xaxis100;
                    cek.y = response.kompetensi.nilai_detail_box[0].yaxis100;
                    var categori_box = cek_box_categori(cek);
                    var nilai_x = (Math.round(response.kompetensi.nilai_detail_box[0].xaxis100 * 100) / 100)
                        .toFixed(2)
                    var nilai_y = (Math.round(response.kompetensi.nilai_detail_box[0].yaxis100 * 100) / 100)
                        .toFixed(2)

                    var data = {
                        "DT_RowId": response.kepegawaian[0].nip,
                        "name": response.kepegawaian[0].nama,
                        "x": nilai_x,
                        "y": nilai_y,
                        "categori": categori_box,

                    }
                    datatable.push(data);
                    tabelPegawai.rows.add(datatable).draw();
                    //set series on profile
                    $('#categori_box').text(categori_box);
                    $('#judul_tabel').text(categori_box);

                    //draw tabel detail pada penilaian diklat
                    draw_tabel_diklat(response.diklat)
                    //draw tabel detail pada penilaian pendidikan
                    draw_tabel_pendidikan(response.pendidikan)
                    //draw tabel bahasa
                    draw_tabel_bahasa(response.bahasa)
                    //draw tabel vote
                    draw_tabel_vote(response)
                    //draw tabel eotm
                    draw_tabel_eotm(response.eotm)
                    //drawo modal/tabel skp
                    draw_tabel_skp(response.nilai_skp_bulanan);
                    //draw modal/tabel absensi
                    draw_tabel_absensi(response.rekap_hadir,response.rekap_akumulasi)
                   
                    //scroll down to data detail
                    $('html, body').animate({
                        scrollTop: $("#hasil_uji_kompetensi").offset().top
                    }, 100);


                } else {
                    arr_kompetensi = [];

                    $('#name_on_grap').text("");
                }

                //set to tabel
                add_data_tabel(arr_kompetensi);

            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('error');
            }
        })
    }

    function draw_tabel_diklat(data_diklat) {

        var datadiklat = [];
        tabelDiklat.clear().draw();
        if (data_diklat.diklat_fungsional) {
            $.each(data_diklat.diklat_fungsional, function(key, value) {
                var data = {
                    "name": value.Nama_Diklat,
                    "instansi": value.Instansi,
                    "tanggal": changeDate(value.TGL_Ijazah),

                }
                datadiklat.push(data);
            })

        }
        if (data_diklat.diklat_struktural) {
            $.each(data_diklat.diklat_struktural, function(key, value) {
                var data = {
                    "name": value.Nama_Diklat,
                    "instansi": value.Instansi,
                    "tanggal": changeDate(value.TGL_Ijazah),

                }
                datadiklat.push(data);
            })
        }
        if (data_diklat.seminar) {
            $.each(data_diklat.seminar, function(key, value) {
                var data = {
                    "name": value.Nama_Seminar,
                    "instansi": value.Instansi,
                    "tanggal": changeDate(value.TGL_Ijazah),

                }
                datadiklat.push(data);
            })
        }
        // console.log(datadiklat)
        tabelDiklat.rows.add(datadiklat).draw();

    }

    function draw_tabel_pendidikan(data_pendidikan) {
        var pendidikan = [];
        tabelPendidikan.clear().draw();
        if (data_pendidikan) {
            $.each(data_pendidikan, function(key, value) {
                var tanggal = "-"
                if (value.TGL_Ijazah != null) {
                    tanggal = changeDate(value.TGL_Ijazah);
                }
                var data = {
                    "name": value.Tingkat_Pendidikan,
                    "instansi": value.Nama_Sekolah,
                    "tanggal": tanggal,

                }
                pendidikan.push(data);
            })

        }
        tabelPendidikan.rows.add(pendidikan).draw();

    }

    function draw_tabel_bahasa(data_bahasa) {
        var bahasa = [];
        tabelBahasa.clear().draw();
        if (data_bahasa) {
            $.each(data_bahasa, function(key, value) {
                var data = [value.Jenis_Kemampuan_Bahasa, value.Instansi, changeDate(value.TGL_Sertifikat), value
                    .Nilai_Angka
                ]
                bahasa.push(data);
            })

        }
        tabelBahasa.rows.add(bahasa).draw();

    }

    function draw_tabel_vote(data) {
        var data_chart = []
        html = ""
        vote = data.vote
        if (vote.length > 0) {
            nilai_peer = (Math.round(data.vote[0].nilai_peer * 100) / 100).toFixed(2)
            nilai_vote = (Math.round(data.vote[0].nilai_vote * 100) / 100).toFixed(2)
            nilai_key_player = (Math.round(data.vote[0].nilai_key_player * 100) / 100).toFixed(2)
            nilai_akhir = (Math.round(data.vote[0].nilai_akhir * 100) / 100).toFixed(2)
            y_vote_50 = (Math.round(data.kompetensi.nilai_detail_box[0].y_vote_50 * 100) / 100).toFixed(2)
        } else {
            nilai_peer = 0
            nilai_vote = 0
            nilai_key_player = 0
            nilai_akhir = 0
            y_vote_50 = 0
        }

        html = html + `<tr><td>` + nilai_peer + `</td><td >` + nilai_vote +
            `</td><td >` + nilai_key_player + `</td><td >` + nilai_akhir + `</td><td >` + y_vote_50 + `</td></tr>`
        $('#tabel-vote tbody').empty();
        $('#tabel-vote tbody').append(html);

        $('#peer-review-eotm').text(nilai_peer);
        $('#vote-eotm').text(nilai_vote);
        $('#key-player-eotm').text(nilai_key_player);

        // default data chart
        for (let i = 1; i <= 12; i++) {
            data_chart[i] = 0;
        }


        $.each(data.vote, function(key, value) {
            data_chart[value.month_create] = value.nilai_akhir;
        })

        //draw chart
        root_vote.container.children.clear()
        generate_chart_vote(data_chart);
    }

    function draw_tabel_eotm(data_eotm) {

        var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        var data_chart = []
        if (data_eotm.length > 0) {
            for (let i = 1; i <= 12; i++) {
                var data = {
                    bulan: months[i - 1],
                    value: -50,
                    value2: ""
                };
                data_chart.push(data);
            }
            $.each(data_eotm, function(key, val) {
                data_chart[val.month_create - 1].value = val.rank * -1;
                data_chart[val.month_create - 1].value2 = "Peringkat " + val.rank;
            })
            $('#peringkat-eotm').text(data_eotm[0].rank)

        } else {

            $('#peringkat-eotm').text("-")
        }
        //draw chart
        root_eotm.container.children.clear()
        generate_chart_eotm(data_chart);
    }

    function draw_tabel_skp(data_skp) {

        var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        var data_chart = []
        for (let i = 1; i <= 12; i++) {
            var data = {
                bulan: months[i - 1],
                value: 0,
            };
            data_chart.push(data);
        }
        if (data_skp.length > 0) {
            $.each(data_skp, function(key, val) {

                data_chart[val.bulan - 1].value = val.nilai;
            })

        }
        //draw chart

        root_skp.container.children.clear()
        generate_chart_skp(data_chart);
    }

    function draw_tabel_absensi(data_absensi,akumulasi) {
        var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        var data_chart = []
        for (let i = 1; i <= 12; i++) {
            var data = {
                bulan: months[i - 1],
                hadir: 0,
                tidak_hadir: 0,
                dinas: 0,
                pendidikan: 0,
            };
            data_chart.push(data);
        }
        if (data_absensi.length > 0) {
            $.each(data_absensi, function(key, val) {

                data_chart[val.bulan - 1].hadir = val.hdr + val.hdr_telat;
                data_chart[val.bulan - 1].tidak_hadir = val.alpa + val.cuti;
                data_chart[val.bulan - 1].dinas = val.dinas;
                data_chart[val.bulan - 1].pendidikan = val.diklat + val.tb;
            })

        }
        //draw chart        
        root_absensi.container.children.clear()
        generate_chart_absensi(data_chart);

        //draw akumulasi terlambat
        $('#nominal-akumulasi').text(akumulasi.akumulasi)
        $("#akumulasi_card").removeClass();
        if(akumulasi.jml_akumulas_int>=5 ){
            $("#akumulasi_card").addClass('card l-bg-purple-dark');
        }else if(akumulasi.jml_akumulas_int>=3){
            $("#akumulasi_card").addClass('card l-bg-orange-dark');
        }else{
            $("#akumulasi_card").addClass('card l-bg-green-dark');
        }
    }

    function changeDate(Tanggal) {
        var monthNames = ["Jan", "Feb", "Mar", "Apr",
            "May", "Jun", "Jul", "Aug",
            "Sep", "Oct", "Nov", "Dec"
        ];

        var now = new Date(Tanggal);

        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2) - 1;
        var today = (day) + "-" + monthNames[month] + "-" + now.getFullYear();
        return today
    }
    //untuk add pada tabel detail komponen
    function add_data_tabel(arr_kompetensi) {
        if (Status == 'Pemetaan') {
            data_pembanding = arr_kompetensi.pemetaan
            data_nilai = arr_kompetensi.nilai
        } else {
            data_pembanding = arr_kompetensi.promosi
            data_nilai = arr_kompetensi.nilai
        }

        html = `<tr><td>Integritas</td><td class="text-center">` + data_pembanding[0].nm_integritas +
            `</td><td class="text-center">` + data_nilai[0].nm_integritas + `</td></tr>`
        html = html + `<tr><td>Kerjasama</td><td class="text-center">` + data_pembanding[0].nm_kerjasama +
            `</td><td class="text-center">` + data_nilai[0].nm_kerjasama + `</td></tr>`
        html = html + `<tr><td>Komunikasi</td><td class="text-center">` + data_pembanding[0].nm_komunikasi +
            `</td><td class="text-center">` + data_nilai[0].nm_komunikasi + `</td></tr>`
        html = html + `<tr><td>Orientasi Hasil</td><td class="text-center">` + data_pembanding[0]
            .nm_orientasi_pada_hasil +
            `</td><td class="text-center">` + data_nilai[0].nm_orientasi_pada_hasil + `</td></tr>`
        html = html + `<tr><td>Pelayanan Publik</td><td class="text-center">` + data_pembanding[0]
            .nm_pelayanan_publik +
            `</td><td class="text-center">` + data_nilai[0].nm_pelayanan_publik + `</td></tr>`
        html = html + `<tr><td>Pengembangan</td><td class="text-center">` + data_pembanding[0].nm_pengembangan +
            `</td><td class="text-center">` + data_nilai[0].nm_pengembangan + `</td></tr>`
        html = html + `<tr><td>Mengelola Perubahan</td><td class="text-center">` + data_pembanding[0]
            .nm_mengelola_perubahan + `</td><td class="text-center">` + data_nilai[0].nm_mengelola_perubahan +
            `</td></tr>`
        html = html + `<tr><td>Pengambilan Keputusan</td><td class="text-center">` + data_pembanding[0]
            .nm_pengambilan_keputusan + `</td><td class="text-center">` + data_nilai[0].nm_pengambilan_keputusan +
            `</td></tr>`
        html = html + `<tr><td>Perekat Bangsa</td><td class="text-center">` + data_pembanding[0].ns_perekat_bangsa +
            `</td><td class="text-center">` + data_nilai[0].ns_perekat_bangsa + `</td></tr>`
        html = html + `<tr><td><b>Total</b></td><td class="text-center"><b>` + data_pembanding[0].total +
            `</b></td><td class="text-center"><b>` + data_nilai[0].total_nilai + `</b></td></tr>`
        $('#tabel-pemetaan tbody').empty();
        $('#tabel-pemetaan tbody').append(html);

    }

    function initiate_chart_radar() {
        root_radar = am5.Root.new("chart_radar");

        // Set themes
        // https://www.amcharts.com/docs/v5/concepts/themes/
        root_radar.setThemes([
            am5themes_Animated.new(root_radar)
        ]);
        // Create axes and their renderers
        // https://www.amcharts.com/docs/v5/charts/radar-chart/#Adding_axes
        xRenderer_radar = am5radar.AxisRendererCircular.new(root_radar, {
            minGridDistance: 30
        });
        xRenderer_radar.labels.template.setAll({
            textType: "adjusted",
            radius: 10,
            paddingTop: 0,
            paddingBottom: 0,
            centerY: am5.p50,
            fontSize: "0.8em"
        });

        xRenderer_radar.grid.template.setAll({
            location: 0.5,
            // strokeDasharray: [2, 2]
        });

        // Create chart
        // https://www.amcharts.com/docs/v5/charts/radar-chart/
        chart_radar = root_radar.container.children.push(
            am5radar.RadarChart.new(root_radar, {
                panX: false,
                panY: false,
                /* wheelX: "panX",
                    wheelY: "zoomX", */
                innerRadius: am5.percent(40),
                radius: am5.percent(70),
                arrangeTooltips: false
            })
        );

        // Add cursor
        // https://www.amcharts.com/docs/v5/charts/radar-chart/#Cursor
        cursor_radar = chart_radar.set("cursor", am5radar.RadarCursor.new(root_radar, {
            /* behavior: "zoomX" */
        }));

        cursor_radar.lineY.set("visible", false);



        xAxis_radar = chart_radar.xAxes.push(
            am5xy.CategoryAxis.new(root_radar, {
                maxDeviation: 0,
                categoryField: "name",
                renderer: xRenderer_radar,
                tooltip: am5.Tooltip.new(root_radar, {})
            })
        );

        var yRenderer = am5radar.AxisRendererRadial.new(root_radar, {
            minGridDistance: 30
        });

        yAxis_radar = chart_radar.yAxes.push(
            am5xy.ValueAxis.new(root_radar, {
                renderer: yRenderer
            })
        );

        yRenderer.grid.template.setAll({
            strokeDasharray: [2, 2]
        });

        // Create series
        // https://www.amcharts.com/docs/v5/charts/radar-chart/#Adding_series
        series1_radar = chart_radar.series.push(
            am5radar.RadarLineSeries.new(root_radar, {
                name: "Hasil",
                xAxis: xAxis_radar,
                yAxis: yAxis_radar,
                valueYField: "value1",
                categoryXField: "name"
            })
        );

        series1_radar.strokes.template.setAll({
            strokeOpacity: 0
        });

        series1_radar.fills.template.setAll({
            visible: true,
            fillOpacity: 0.5
        });

        series2_radar = chart_radar.series.push(
            am5radar.RadarLineSeries.new(root_radar, {
                name: "Pemetaan",
                xAxis: xAxis_radar,
                yAxis: yAxis_radar,
                valueYField: "value2",
                categoryXField: "name",
                stacked: false,
                tooltip: am5.Tooltip.new(root_radar, {
                    labelText: "Pemetaan: {value1}\nHasil:{value2}"
                })
            })
        );

        series2_radar.strokes.template.setAll({
            strokeOpacity: 0
        });

        series2_radar.fills.template.setAll({
            visible: true,
            fillOpacity: 0.5
        });

        var legend = chart_radar.radarContainer.children.push(
            am5.Legend.new(root_radar, {
                width: 150,
                centerX: am5.p50,
                centerY: am5.p50
            })
        );
        legend.data.setAll([series1_radar, series2_radar]);

        // Set data
        // https://www.amcharts.com/docs/v5/charts/radar-chart/#Setting_data
        var data = [{
                name: "Integritas",
                value1: 0,
                value2: 0,
            },
            {
                name: "Kerja Sama",
                value1: 0,
                value2: 0,
            },
            {
                name: "Komunikasi",
                value1: 0,
                value2: 0,
            },
            {
                name: "Orientasi Hasil",
                value1: 0,
                value2: 0,
            },
            {
                name: "Pelayanan Publik",
                value1: 0,
                value2: 0,
            },
            {
                name: "Pengembangan",
                value1: 0,
                value2: 0,
            },
            {
                name: "Pengelolaan Perubahan",
                value1: 0,
                value2: 0,
            },
            {
                name: "Pengambilan Keputusan",
                value1: 0,
                value2: 0,
            },
            {
                name: "Perekat bangsa",
                value1: 0,
                value2: 0,
            }
        ];

        series1_radar.data.setAll(data);
        series2_radar.data.setAll(data);
        xAxis_radar.data.setAll(data);

        // Animate chart and series in
        // https://www.amcharts.com/docs/v5/concepts/animations/#Initial_animation

        series1_radar.appear(1000);
        series2_radar.appear(1000);
        chart_radar.appear(1000, 100);
    }

    // FUNCTION UNTUK GENERATE RADAR
    // generate_chart_radar({data},{value filter})
    function generate_chart_radar(kompetensi, komponen) {

        // Create axes and their renderers
        // https://www.amcharts.com/docs/v5/charts/radar-chart/#Adding_axes
        root_radar.container.children.clear()

        xRenderer_radar = am5radar.AxisRendererCircular.new(root_radar, {
            minGridDistance: 30
        });
        xRenderer_radar.labels.template.setAll({
            textType: "adjusted",
            radius: 10,
            paddingTop: 0,
            paddingBottom: 0,
            centerY: am5.p50,
            fontSize: "0.8em"
        });

        xRenderer_radar.grid.template.setAll({
            location: 0.5,
            // strokeDasharray: [2, 2]
        });
        // Create chart
        // https://www.amcharts.com/docs/v5/charts/radar-chart/
        chart_radar = root_radar.container.children.push(
            am5radar.RadarChart.new(root_radar, {
                panX: false,
                panY: false,
                /* wheelX: "panX",
                    wheelY: "zoomX", */
                innerRadius: am5.percent(40),
                radius: am5.percent(70),
                arrangeTooltips: false,

            })
        );

        // Add cursor
        // https://www.amcharts.com/docs/v5/charts/radar-chart/#Cursor
        cursor_radar = chart_radar.set("cursor", am5radar.RadarCursor.new(root_radar, {
            /* behavior: "zoomX" */
        }));

        cursor_radar.lineY.set("visible", false);



        xAxis_radar = chart_radar.xAxes.push(
            am5xy.CategoryAxis.new(root_radar, {
                maxDeviation: 0,
                categoryField: "name",
                renderer: xRenderer_radar,
                tooltip: am5.Tooltip.new(root_radar, {})
            })
        );
        var yRenderer = am5radar.AxisRendererRadial.new(root_radar, {
            minGridDistance: 30
        });

        yAxis_radar = chart_radar.yAxes.push(
            am5xy.ValueAxis.new(root_radar, {
                renderer: yRenderer
            })
        );

        yRenderer.grid.template.setAll({
            strokeDasharray: [2, 2]
        });

        // Create series
        // https://www.amcharts.com/docs/v5/charts/radar-chart/#Adding_series
        series1_radar = chart_radar.series.push(
            am5radar.RadarLineSeries.new(root_radar, {
                name: komponen,
                xAxis: xAxis_radar,
                yAxis: yAxis_radar,
                valueYField: "value1",
                categoryXField: "name",
                fill: am5.color("#f6cd61"),
            })
        );

        series1_radar.strokes.template.setAll({
            strokeOpacity: 0
        });

        series1_radar.fills.template.setAll({
            visible: true,
            fillOpacity: 0.5
        });

        series2_radar = chart_radar.series.push(
            am5radar.RadarLineSeries.new(root_radar, {
                name: "Hasil",
                xAxis: xAxis_radar,
                yAxis: yAxis_radar,
                valueYField: "value2",
                categoryXField: "name",
                stacked: false,
                fill: am5.color("#fe8a71"),
                tooltip: am5.Tooltip.new(root_radar, {
                    labelText: komponen + ": {value1}\nHasil:{value2}"
                })
            })
        );

        series2_radar.strokes.template.setAll({
            strokeOpacity: 0
        });

        series2_radar.fills.template.setAll({
            visible: true,
            fillOpacity: 0.5
        });
        // var legend = chart_radar.radarContainer.children.push(
        //     am5.Legend.new(root_radar, {
        //         width: 150,
        //         centerX: am5.p50,
        //         centerY: am5.p50
        //     })
        // );
        var legend = chart_radar.children.push(am5.Legend.new(root_radar, {}));

        legend.data.setAll([series1_radar, series2_radar]);

        // Set data
        // https://www.amcharts.com/docs/v5/charts/radar-chart/#Setting_data

        if (kompetensi != null) {
            if (komponen == "Pemetaan") {
                var data_nilai = [{
                        name: "Integritas",
                        value1: kompetensi.pemetaan[0].nm_integritas,
                        value2: kompetensi.nilai[0].nm_integritas,
                    },
                    {
                        name: "Kerja Sama",
                        value1: kompetensi.pemetaan[0].nm_kerjasama,
                        value2: kompetensi.nilai[0].nm_kerjasama,
                    },
                    {
                        name: "Komunikasi",
                        value1: kompetensi.pemetaan[0].nm_komunikasi,
                        value2: kompetensi.nilai[0].nm_komunikasi,
                    },
                    {
                        name: "Mengelola Perubahan",
                        value1: kompetensi.pemetaan[0].nm_mengelola_perubahan,
                        value2: kompetensi.nilai[0].nm_mengelola_perubahan,
                    },
                    {
                        name: "Orientasi Hasil",
                        value1: kompetensi.pemetaan[0].nm_orientasi_pada_hasil,
                        value2: kompetensi.nilai[0].nm_orientasi_pada_hasil,
                    },
                    {
                        name: "Pelayanan Publik",
                        value1: kompetensi.pemetaan[0].nm_pelayanan_publik,
                        value2: kompetensi.nilai[0].nm_pelayanan_publik,
                    },
                    {
                        name: "Pengambilan Keputusan",
                        value1: kompetensi.pemetaan[0].nm_pengambilan_keputusan,
                        value2: kompetensi.nilai[0].nm_pengambilan_keputusan,
                    },
                    {
                        name: "Pengembangan",
                        value1: kompetensi.pemetaan[0].nm_pengembangan,
                        value2: kompetensi.nilai[0].nm_pengembangan,
                    },
                    {
                        name: "Perekat bangsa",
                        value1: kompetensi.pemetaan[0].ns_perekat_bangsa,
                        value2: kompetensi.nilai[0].ns_perekat_bangsa,
                    },
                ];
            } else {
                //ketika promosi
                var data_nilai = [{
                        name: "Integritas",
                        value1: kompetensi.promosi[0].nm_integritas,
                        value2: kompetensi.nilai[0].nm_integritas,
                    },
                    {
                        name: "Kerja Sama",
                        value1: kompetensi.promosi[0].nm_kerjasama,
                        value2: kompetensi.nilai[0].nm_kerjasama,
                    },
                    {
                        name: "Komunikasi",
                        value1: kompetensi.promosi[0].nm_komunikasi,
                        value2: kompetensi.nilai[0].nm_komunikasi,
                    },
                    {
                        name: "Mengelola Perubahan",
                        value1: kompetensi.promosi[0].nm_mengelola_perubahan,
                        value2: kompetensi.nilai[0].nm_mengelola_perubahan,
                    },
                    {
                        name: "Orientasi Hasil",
                        value1: kompetensi.promosi[0].nm_orientasi_pada_hasil,
                        value2: kompetensi.nilai[0].nm_orientasi_pada_hasil,
                    },
                    {
                        name: "Pelayanan Publik",
                        value1: kompetensi.promosi[0].nm_pelayanan_publik,
                        value2: kompetensi.nilai[0].nm_pelayanan_publik,
                    },
                    {
                        name: "Pengambilan Keputusan",
                        value1: kompetensi.promosi[0].nm_pengambilan_keputusan,
                        value2: kompetensi.nilai[0].nm_pengambilan_keputusan,
                    },
                    {
                        name: "Pengembangan",
                        value1: kompetensi.promosi[0].nm_pengembangan,
                        value2: kompetensi.nilai[0].nm_pengembangan,
                    },
                    {
                        name: "Perekat bangsa",
                        value1: kompetensi.promosi[0].ns_perekat_bangsa,
                        value2: kompetensi.nilai[0].ns_perekat_bangsa,
                    },
                ];
            }
        } else {
            var data_nilai = [];
        }
        // console.log(data_nilai)
        // console.log(kompetensi)
        series1_radar.data.setAll(data_nilai);
        series2_radar.data.setAll(data_nilai);
        xAxis_radar.data.setAll(data_nilai);

        // Animate chart and series in
        // https://www.amcharts.com/docs/v5/concepts/animations/#Initial_animation

        series1_radar.appear(1000);
        series2_radar.appear(1000);
        chart_radar.appear(1000, 100);
    }


    function generate_chart_vote(data_arr) {

        // Set themes
        // https://www.amcharts.com/docs/v5/concepts/themes/
        root_vote.setThemes([
            am5themes_Animated.new(root_vote)
        ]);


        // Create chart
        // https://www.amcharts.com/docs/v5/charts/xy-chart/
        var chart_vote = root_vote.container.children.push(am5xy.XYChart.new(root_vote, {
            panX: true,
            panY: true,
            // wheelX: "panX",
            // wheelY: "zoomX",
            pinchZoomX: true
        }));

        // Add cursor
        // https://www.amcharts.com/docs/v5/charts/xy-chart/cursor/
        var cursor = chart_vote.set("cursor", am5xy.XYCursor.new(root_vote, {}));
        cursor.lineY.set("visible", false);


        // Create axes
        // https://www.amchart_votes.com/docs/v5/chart_votes/xy-chart_vote/axes/
        var xRenderer = am5xy.AxisRendererX.new(root_vote, {
            minGridDistance: 30
        });
        xRenderer.labels.template.setAll({

            centerY: am5.p50,
            centerX: am5.p100,
            paddingRight: 15,

        });

        var xAxis = chart_vote.xAxes.push(am5xy.CategoryAxis.new(root_vote, {
            maxDeviation: 0.3,
            categoryField: "bulan",
            renderer: xRenderer,
            tooltip: am5.Tooltip.new(root_vote, {})
        }));

        var yAxis = chart_vote.yAxes.push(am5xy.ValueAxis.new(root_vote, {
            maxDeviation: 0.3,
            min: 0,
            max: 100,

            renderer: am5xy.AxisRendererY.new(root_vote, {})
        }));


        // Create series
        // https://www.amchart_votes.com/docs/v5/chart_votes/xy-chart_vote/series/
        var series = chart_vote.series.push(
            am5xy.LineSeries.new(root_vote, {
                name: "Series 1",
                xAxis: xAxis,
                yAxis: yAxis,
                valueYField: "value",
                sequencedInterpolation: true,
                categoryXField: "bulan",


            })
        );
        series.bullets.push(function() {
            var circle = am5.Circle.new(root_vote, {
                radius: 7,
                fill: series.get("fill"),
                stroke: root_vote.interfaceColors.get("background"),
                strokeWidth: 2,
                tooltipText: "{valueY}"
            });

            return am5.Bullet.new(root_vote, {
                sprite: circle
            });
        });


        // Set data
        var data = [{
            bulan: "Jan",
            value: data_arr[1]
        }, {
            bulan: "Feb",
            value: data_arr[2]
        }, {
            bulan: "Mar",
            value: data_arr[3]
        }, {
            bulan: "Apr",
            value: data_arr[4]
        }, {
            bulan: "Mei",
            value: data_arr[5]
        }, {
            bulan: "Jun",
            value: data_arr[6]
        }, {
            bulan: "Jul",
            value: data_arr[7]
        }, {
            bulan: "Aug",
            value: data_arr[8]
        }, {
            bulan: "Sep",
            value: data_arr[9]
        }, {
            bulan: "Oct",
            value: data_arr[10]
        }, {
            bulan: "Nov",
            value: data_arr[11]
        }, {
            bulan: "Des",
            value: data_arr[12]
        }];

        xAxis.data.setAll(data);
        series.data.setAll(data);


        // Make stuff animate on load
        // https://www.amchart_votes.com/docs/v5/concepts/animations/
        series.appear(1000);
        chart_vote.appear(1000, 100);

    }


    function generate_chart_eotm(data_chart) {
        // Set themes
        // https://www.amcharts.com/docs/v5/concepts/themes/
        root_eotm.setThemes([
            am5themes_Animated.new(root_eotm)
        ]);


        // Create chart
        // https://www.amcharts.com/docs/v5/charts/xy-chart/
        var chart_eotm = root_eotm.container.children.push(am5xy.XYChart.new(root_eotm, {
            panX: true,
            panY: true,
            // wheelX: "panX",
            // wheelY: "zoomX",
            pinchZoomX: true
        }));

        // Add cursor
        // https://www.amcharts.com/docs/v5/charts/xy-chart/cursor/
        var cursor = chart_eotm.set("cursor", am5xy.XYCursor.new(root_eotm, {}));
        cursor.lineY.set("visible", false);


        // Create axes
        // https://www.amchart_eotms.com/docs/v5/chart_eotms/xy-chart_eotm/axes/
        var xRenderer = am5xy.AxisRendererX.new(root_eotm, {
            minGridDistance: 30
        });
        xRenderer.labels.template.setAll({

            centerY: am5.p50,
            centerX: am5.p100,
            paddingRight: 15,

        });

        var xAxis = chart_eotm.xAxes.push(am5xy.CategoryAxis.new(root_eotm, {
            maxDeviation: 0.3,
            categoryField: "bulan",
            renderer: xRenderer,
            tooltip: am5.Tooltip.new(root_eotm, {})
        }));

        var yAxis = chart_eotm.yAxes.push(am5xy.ValueAxis.new(root_eotm, {
            maxDeviation: 0.3,
            max: 0,
            min: -50,
            visible: false,
            renderer: am5xy.AxisRendererY.new(root_eotm, {})
        }));


        // Create series
        // https://www.amchart_eotms.com/docs/v5/chart_eotms/xy-chart_eotm/series/
        var series = chart_eotm.series.push(
            am5xy.LineSeries.new(root_eotm, {
                name: "Series 1",
                xAxis: xAxis,
                yAxis: yAxis,
                valueYField: "value",
                sequencedInterpolation: true,
                categoryXField: "bulan",


            })
        );
        series.bullets.push(function() {
            var circle = am5.Circle.new(root_eotm, {
                radius: 7,
                fill: series.get("fill"),
                stroke: root_eotm.interfaceColors.get("background"),
                strokeWidth: 2,
                tooltipText: "{value2}"
            });

            return am5.Bullet.new(root_eotm, {
                sprite: circle
            });
        });


        // Set data
        // console.log(data_chart[1])
        var data = [{
            bulan: "Jan",
            value: 0,
            value2: "Peringkat 1" + data_chart[1]
        }, {
            bulan: "Feb",
            value: -32,
            value2: "Peringkat 32"
        }, {
            bulan: "Mar",
            value: -4,
            value2: "Peringkat 4"
        }, {
            bulan: "Apr",
            value: -5,
            value2: "Peringkat 5"
        }, {
            bulan: "Mei",
            value: -6,
            value2: "Peringkat 6"
        }, {
            bulan: "Jun",
            value: -12,
            value2: "Peringkat 12"
        }, {
            bulan: "Jul",
            value: -2,
            value2: "Peringkat 2"
        }, {
            bulan: "Aug",
            value: -7,
            value2: "Peringkat 7"
        }, {
            bulan: "Sep",
            value: -6,
            value2: "Peringkat 6"
        }, {
            bulan: "Oct",
            value: -9,
            value2: "Peringkat 9"
        }, {
            bulan: "Nov",
            value: -5,
            value2: "Peringkat 5"
        }, {
            bulan: "Des",
            value: -4,
            value2: "Peringkat 4"
        }];

        xAxis.data.setAll(data_chart);
        series.data.setAll(data_chart);


        // Make stuff animate on load
        // https://www.amchart_eotms.com/docs/v5/concepts/animations/
        series.appear(1000);
        chart_eotm.appear(1000, 100);

    }
    //generate chart bar skp
    function generate_chart_skp(data_skp) {
        // console.log(data_skp)
        // Set themes
        // https://www.amcharts.com/docs/v5/concepts/themes/


        root_skp.setThemes([
            am5themes_Animated.new(root_skp)
        ]);


        // Create chart
        // https://www.amcharts.com/docs/v5/charts/xy-chart/
        var chart_skp = root_skp.container.children.push(am5xy.XYChart.new(root_skp, {
            //panX: true,
            //panY: true,
            //wheelX: "panX",
            //wheelY: "zoomX",
            pinchZoomX: true
        }));

        // Add cursor
        // https://www.amchart_skps.com/docs/v5/chart_skps/xy-chart_skp/cursor/
        var cursor = chart_skp.set("cursor", am5xy.XYCursor.new(root_skp, {}));
        cursor.lineY.set("visible", false);


        // Create axes
        // https://www.amchart_skps.com/docs/v5/chart_skps/xy-chart_skp/axes/
        var xRenderer = am5xy.AxisRendererX.new(root_skp, {
            minGridDistance: 30
        });
        xRenderer.labels.template.setAll({
            rotation: -90,
            centerY: am5.p50,
            centerX: am5.p100,
            paddingRight: 15
        });

        var xAxis = chart_skp.xAxes.push(am5xy.CategoryAxis.new(root_skp, {
            maxDeviation: 0.3,
            categoryField: "bulan",
            renderer: xRenderer,
            tooltip: am5.Tooltip.new(root_skp, {})
        }));

        var yAxis = chart_skp.yAxes.push(am5xy.ValueAxis.new(root_skp, {
            maxDeviation: 0.3,
            renderer: am5xy.AxisRendererY.new(root_skp, {})
        }));


        // Create series
        // https://www.amchart_skps.com/docs/v5/chart_skps/xy-chart_skp/series/
        var series_skp = chart_skp.series.push(am5xy.ColumnSeries.new(root_skp, {
            name: "Series 1",
            xAxis: xAxis,
            yAxis: yAxis,
            valueYField: "value",
            sequencedInterpolation: true,
            categoryXField: "bulan",
        }));

        series_skp.bullets.push(function() {
            return am5.Bullet.new(root_skp, {
                locationY: 1,
                sprite: am5.Label.new(root_skp, {
                    text: "{valueYWorking.formatNumber('#.')}",
                    fill: root_skp.interfaceColors.get("alternativeText"),
                    centerY: 0,
                    centerX: am5.p50,
                    populateText: true
                })
            });
        });

        series_skp.columns.template.setAll({
            cornerRadiusTL: 5,
            cornerRadiusTR: 5
        });
        series_skp.columns.template.adapters.add("fill", function(fill, target) {
            return chart_skp.get("colors").getIndex(series_skp.columns.indexOf(target));
        });

        series_skp.columns.template.adapters.add("stroke", function(stroke, target) {
            return chart_skp.get("colors").getIndex(series_skp.columns.indexOf(target));
        });




        xAxis.data.setAll(data_skp);
        series_skp.data.setAll(data_skp);


        // Make stuff animate on load
        // https://www.amchart_skps.com/docs/v5/concepts/animations/
        series_skp.appear(1000);
        chart_skp.appear(1000, 100);

    }

    //generate chart bar absensi
    function generate_chart_absensi(data_absensi) {
        root_absensi.setThemes([
            am5themes_Animated.new(root_absensi)
        ]);


        // Create chart
        // https://www.amcharts.com/docs/v5/charts/xy-chart/
        var chart = root_absensi.container.children.push(am5xy.XYChart.new(root_absensi, {
            panX: false,
            panY: false,
            /* wheelX: "panX",
            wheelY: "zoomX", */
            layout: root_absensi.verticalLayout
        }));


        // Add legend
        // https://www.amcharts.com/docs/v5/charts/xy-chart/legend-xy-series/
        var legend = chart.children.push(am5.Legend.new(root_absensi, {
            centerX: am5.p50,
            x: am5.p50
        }));

        var data = [{
                "bulan": "Jan",
                "hadir": 0,
                "tidak_hadir": 0,
                "dinas": 0,
                "pendidikan": 0,
            }, {
                "bulan": "Feb",
                "hadir": 0,
                "tidak_hadir": 0,
                "dinas": 0,
                "pendidikan": 0,
            }, {
                "bulan": "Mar",
                "hadir": 0,
                "tidak_hadir": 0,
                "dinas": 0,
                "pendidikan": 0,
            },
            {
                "bulan": "Apr",
                "hadir": 0,
                "tidak_hadir": 0,
                "dinas": 0,
                "pendidikan": 0,
            },
            {
                "bulan": "May",
                "hadir": 0,
                "tidak_hadir": 0,
                "dinas": 0,
                "pendidikan": 0,
            },
            {
                "bulan": "Jun",
                "hadir": 0,
                "tidak_hadir": 0,
                "dinas": 0,
                "pendidikan": 0,
            },
            {
                "bulan": "Jul",
                "hadir": 0,
                "tidak_hadir": 0,
                "dinas": 0,
                "pendidikan": 0,
            },
            {
                "bulan": "Aug",
                "hadir": 0,
                "tidak_hadir": 0,
                "dinas": 0,
                "pendidikan": 0,
            },
            {
                "bulan": "Sep",
                "hadir": 0,
                "tidak_hadir": 0,
                "dinas": 0,
                "pendidikan": 0,
            },
            {
                "bulan": "Oct",
                "hadir": 0,
                "tidak_hadir": 0,
                "dinas": 0,
                "pendidikan": 0,
            },
            {
                "bulan": "Nov",
                "hadir": 0,
                "tidak_hadir": 0,
                "dinas": 0,
                "pendidikan": 0,
            },
            {
                "bulan": "Des",
                "hadir": 0,
                "tidak_hadir": 0,
                "dinas": 0,
                "pendidikan": 0,
            }

        ];


        // Create axes
        // https://www.amcharts.com/docs/v5/charts/xy-chart/axes/
        var xAxis = chart.xAxes.push(am5xy.CategoryAxis.new(root_absensi, {
            categoryField: "bulan",
            renderer: am5xy.AxisRendererX.new(root_absensi, {
                cellStartLocation: 0.1,
                cellEndLocation: 0.9,
                minGridDistance: 30
            }),
            tooltip: am5.Tooltip.new(root_absensi, {})
        }));


        if (data_absensi.length > 0) {
            xAxis.data.setAll(data_absensi);
        } else {
            xAxis.data.setAll(data);
        }

        var yAxis = chart.yAxes.push(am5xy.ValueAxis.new(root_absensi, {
            min: 0,
            renderer: am5xy.AxisRendererY.new(root_absensi, {})
        }));


        // Add series
        // https://www.amcharts.com/docs/v5/charts/xy-chart/series/
        function makeSeries(name, fieldName, stacked) {
            var series = chart.series.push(am5xy.ColumnSeries.new(root_absensi, {
                stacked: stacked,
                name: name,
                xAxis: xAxis,
                yAxis: yAxis,
                valueYField: fieldName,
                categoryXField: "bulan"
            }));

            series.columns.template.setAll({
                tooltipText: "{name} : {valueY}",
                width: am5.percent(90),
                tooltipY: am5.percent(10),

            });
            if (data_absensi.length > 0) {
                series.data.setAll(data_absensi);
            } else {
                series.data.setAll(data);
            }

            // Make stuff animate on load
            // https://www.amcharts.com/docs/v5/concepts/animations/
            series.appear();

            // series.bullets.push(function() {
            //     return am5.Bullet.new(root_absensi, {
            //         locationY: 0.5,
            //         sprite: am5.Label.new(root_absensi, {
            //             text: "{valueY}",

            //             fill: root_absensi.interfaceColors.get("alternativeText"),
            //             centerY: am5.percent(50),
            //             centerX: am5.percent(50),
            //             populateText: true
            //         })
            //     });
            // });

            legend.data.push(series);
        }

        makeSeries("Hadir", "hadir", true);
        makeSeries("Tidak Hadir", "tidak_hadir", true);
        makeSeries("Dinas", "dinas", true);
        makeSeries("Pendidikan", "pendidikan", true);
        /* makeSeries("Asia", "asia", false);
        makeSeries("Latin America", "lamerica", true);
        makeSeries("Middle East", "meast", true);
        makeSeries("Africa", "africa", true); */


        // Make stuff animate on load
        // https://www.amcharts.com/docs/v5/concepts/animations/
        chart.appear(1000, 100);

    }


    function get_data(url, unit_kerja) {
        test_dinamic_data = []
        default_data = []


        //         if()
        // removeSeries(chart)
        $.ajax({
            type: "POST", // Method pengiriman data bisa dengan GET atau POST,
            data: {
                unit_kerja: unit_kerja,
            },
            url: url, // Isi dengan url/path file php yang dituju
            dataType: "json",
            beforeSend: function() {
                $('#filter-unit-kerja').attr('disabled', true);
            },
            success: function(response) {
                $('#select_all').prop("checked", true);
                $('#filter-unit-kerja').attr('disabled', false);
                if (response.length != 0) {
                    grouping = groupBy(response, "kelompok_jabatan_detail")
                    $.each(grouping, function(key, value) {
                        formated_data = fileter_data(value, key)
                        test_dinamic_data.push(formated_data)
                        default_data.push(formated_data)
                    })
                    // console.log(test_dinamic_data)
                    //generate chart harus  ready am5 chart
                    if (chart) {
                        removeSeries(chart)
                    }
                    generate_chart(default_data)
                    // generate_chart_bar()
                    // var data_null =[];
                    //remove / initiate box data
                    remove_box_data()
                    //generate data on tabel
                    $.each(test_dinamic_data, function(key, value) {
                        view_on_list(value)
                    })
                    // console.log(datatable)
                    // console.log(datatable)
                    tabelPegawai.rows.add(datatable).draw();
                    //penambahan total
                    tabel_list = `<tr><td><b>Total</b></td><td class="text-center"><b>` +
                        total_data_series +
                        `</b></td></tr>`
                    $('#tabel-list-by-jabatan tbody').append(tabel_list);
                } else {

                    if (chart) {
                        removeSeries(chart)
                    }
                    generate_chart(default_data)

                    remove_box_data()
                    //generate data on tabel
                    $('#tabel-list-by-jabatan tbody').empty()
                    // console.log(datatable)
                    // console.log(datatable)
                    tabelPegawai.rows.add(datatable).draw();
                    //penambahan total
                    tabel_list = `<tr><td><b>Total</b></td><td class="text-center"><b>` +
                        total_data_series +
                        `</b></td></tr>`
                    $('#tabel-list-by-jabatan tbody').append(tabel_list);
                    filter_by_box(response)
                }





            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('error');

                $('#filter-unit-kerja').attr('disabled', false);
            }
        })
    }
    am5.ready(function() {
        //awal load hiden personal data
        $('#personal_detail').hide();
        //hiden hasil uji kompetensi
        $('#hasil_uji_kompetensi').hide();
        // $('#filter-unit-kerja').select2();

        //initiate chart radar
        initiate_chart_radar()
        // initiate chart bullet
        initiate_chart()

        // console.log(get_by_nip(detail_data,'197005121994032002'));
        arr_kompetensi = null;
        const api_url = "http://paspeg.kemendag.go.id/manajemen_talenta/neun_box/load/neun/all";
        get_data(api_url, '')


        //function untuk rbah scale pemetaan promosi
        //scale manual input number
        $('#change_level').on('change', function() {
            var data = arr_kompetensi
            if (data != null) {
                if ($(this).val() == "Promosi") {
                    Status = "Promosi"
                    removeSeries(chart_radar)
                    generate_chart_radar(arr_kompetensi, Status)
                    add_data_tabel(arr_kompetensi)
                    // $(this).text('Pemetaan')
                } else {
                    Status = "Pemetaan"

                    removeSeries(chart_radar)
                    generate_chart_radar(arr_kompetensi, Status)
                    add_data_tabel(arr_kompetensi)
                    // $(this).text('Promosi')
                }
            } else {
                $(this).val("Pemetaan")
            }

        });


        tabelPegawai = $('#table-detail').DataTable({
            orderCellsTop: true,
            fixedHeader: true,
            ordering: true,
            paging: true,
            columns: [{
                    data: 'name'
                },
                {
                    data: 'x'
                },
                {
                    data: 'y'
                },
                {
                    data: 'categori'
                }

            ],
            "columnDefs": [{
                "width": "60%",
                "targets": 0
            }],
        })
        tabelPegawai.clear().draw();

        tabelDiklat = $('#tabel-diklat').DataTable({
            orderCellsTop: true,
            fixedHeader: true,
            ordering: true,
            columns: [{
                    data: 'name',
                },
                {
                    data: 'instansi'
                },
                {
                    data: 'tanggal'
                }

            ],

        })
        tabelDiklat.clear().draw();

        tabelPendidikan = $('#tabel-pendidikan').DataTable({
            orderCellsTop: true,
            fixedHeader: true,
            paging: false,
            ordering: false,
            info: false,
            searching: false,
            columns: [{
                    data: 'name',
                },
                {
                    data: 'instansi'
                },
                {
                    data: 'tanggal'
                }

            ],

        })
        tabelPendidikan.clear().draw();
        tabelBahasa = $('#tabel-bahasa').DataTable({
            orderCellsTop: true,
            fixedHeader: true,
            paging: false,
            ordering: false,
            info: false,
            searching: false,


        })

        tabelBahasa.clear().draw();
        //ADD BUTTON FILTER BY NORMAL / KEMENDAG
        $.ajax({
            type: "GET", // Method pengiriman data bisa dengan GET atau POST
            url: "http://paspeg.kemendag.go.id/manajemen_talenta/neun_box/load/skala/all", // Isi dengan url/path file php yang dituju
            dataType: "json",
            success: function(response) {
                generate_button_filter(response[0])
                // console.log(response[0])
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('error ajax function');
            }
        })

        //ADD BUTTON FILTER BY JABATAN
        $.ajax({
            type: "GET", // Method pengiriman data bisa dengan GET atau POST
            url: "http://paspeg.kemendag.go.id/manajemen_talenta/neun_box/load/unit-kerja-filter/all", // Isi dengan url/path file php yang dituju
            dataType: "json",
            success: function(response) {
                generate_filter_unit_kerja(response)
                // console.log(response)
                // console.log(response[0])
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('error ajax function by jabatan');
            }
        })

        //NILAI MAX
        $.ajax({
            type: "GET", // Method pengiriman data bisa dengan GET atau POST
            url: "http://paspeg.kemendag.go.id/manajemen_talenta/neun_box/load/max-variable/all", // Isi dengan url/path file php yang dituju
            dataType: "json",
            success: function(response) {
                $.each(response, function(key, value) {
                    $('#' + value.var_name).text(value.max_value)
                })

            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('error ajax function nilai max');
            }
        })

        //KLICK SHOW ALL Untuk show semua data
        //scale manual input number
        // $('#show_all').on('click', function() {
        //     box_selected = 0
        //     $('#tabel-list-by-jabatan tbody').empty();
        //     $('#table-detail tbody').empty();
        //     datatable = []
        //     remove_box_data()
        //     total_data_series = 0
        //     tabelPegawai.clear().draw()
        //     //generate data on tabel
        //     $.each(test_dinamic_data, function(key, value) {
        //         view_on_list(value)
        //     })
        //     // console.log(datatable)
        //     // console.log(datatable)
        //     tabelPegawai.rows.add(datatable).draw();
        //     //penambahan total
        //     tabel_list = `<tr><td><b>Total</b></td><td class="text-center"><b>` +
        //         total_data_series +
        //         `</b></td></tr>`
        //     $('#tabel-list-by-jabatan tbody').append(tabel_list);
        //     $('#tabel-list-by-box').find('tbody tr').show();

        // });


        // click tabel
        $('#table-detail tbody').on('click', 'tr', function() {
            var nip = $(this).attr('id');
            // $('#loading').modal('show');
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            } else {
                $('#table-detail tbody tr.selected').removeClass('selected')
                $(this).addClass('selected');
                $('html, body').animate({
                    scrollTop: $("#hasil_uji_kompetensi").offset().top
                }, 100);

            }

            get_nilai_by_nip(nip);
        });
        // tabel x y on click
        tabelSumbuX = $('#tabel-sumbu-x').DataTable({
            orderCellsTop: true,
            fixedHeader: true,
            ordering: false,
            paging: false,
            searching: false,
            info: false
        })
        tabelSumbuY = $('#tabel-sumbu-y').DataTable({
            orderCellsTop: true,
            fixedHeader: true,
            ordering: false,
            paging: false,
            searching: false,
            info: false
        })

        var tabelpemetaan = $('#tabel-pemetaan').DataTable({
            orderCellsTop: true,
            fixedHeader: true,
            ordering: false,
            paging: false,
            searching: false,
            info: false
        })

        $('#tabel-sumbu-y').on('click', 'tr', function() {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            } else {
                $('#tabel-sumbu-y tbody tr.selected').removeClass('selected')
                $(this).addClass('selected');

            }
        });
        $('#tabel-sumbu-x').on('click', 'tr', function() {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            } else {
                $('#tabel-sumbu-x tbody tr.selected').removeClass('selected')
                $(this).addClass('selected');

            }
        });

        //detail untuk penilaian
        $('#x_kompetensi_45').parent().on('dblclick', function() {
            $('#modal-view-detail').modal('toggle')
        });
        $('#x_diklat_3').parent().on('dblclick', function() {
            $('#modal-view-detail-diklat').modal('toggle')
        });
        $('#x_pendidikan_9').parent().on('dblclick', function() {
            $('#modal-view-detail-pendidikan').modal('toggle')
        });
        $('#x_bahasa_5').parent().on('dblclick', function() {
            $('#modal-view-detail-bahasa').modal('toggle')
        });
        $('#y_vote_50').parent().on('dblclick', function() {
            $('#modal-view-detail-vote').modal('toggle')

        });
        $('#y_eotm_15').parent().on('dblclick', function() {
            $('#modal-view-detail-eotm').modal('toggle')

        });

        $('#y_absensi_5').parent().on('dblclick', function() {
            $('#modal-view-detail-absensi').modal('toggle')

        });
        $('#y_skp_15').parent().on('dblclick', function() {
            $('#modal-view-detail-skp').modal('toggle')

        });

        ///Start Generate Initiate Chart
        root_vote = am5.Root.new("chart_line_vote");
        root_eotm = am5.Root.new("chart_line_eotm");
        root_skp = am5.Root.new("chart_bar_skp");
        root_absensi = am5.Root.new("chart_bar_absensi");
        // default data chart
        var data_chart = [];
        for (let i = 1; i <= 12; i++) {
            data_chart[i] = 0;
        }
        generate_chart_vote(data_chart);
        //eotm
        var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        var data_chart = []
        for (let i = 1; i <= 12; i++) {
            var data = {
                bulan: months[i - 1],
                value: null,
                value2: ""
            };
            data_chart.push(data);
        }
        generate_chart_eotm(data_chart);
        $("#form_cari_pegawai_nine_box").submit(function(e) {
            e.preventDefault();
            param = $('#cari_pegawai').val()
            find_pegawai(param)
        });

        //skp
        var data_chart = []
        for (let i = 1; i <= 12; i++) {
            var data = {
                bulan: months[i - 1],
                value: '',
            };
            data_chart.push(data);
        }
        generate_chart_skp(data_chart);
        var data_chart = []
        generate_chart_absensi(data_chart);
        //End Generate
        // $('#submit-search').on('click', function() {
        //     param = $('#cari_pegawai').val()
        //     find_pegawai(param)
        // });

        $('#filter-unit-kerja').on('change', function() {
            //set to 0
            arr_kompetensi = [];
            test_dinamic_data = [];
            dinamic_data = [];
            default_data = [];
            all_data = [];
            total_data_series = 0;
            datatable = [];
            tabelPegawai.clear().draw()
            // removeSeries(chart)
            param = this.value
            if (param == 'all') {
                url = "http://paspeg.kemendag.go.id/manajemen_talenta/neun_box/load/neun/all";
                $('#kategori-unit-kerja').text('Unit Kerja : All')

            } else {
                url = "http://paspeg.kemendag.go.id/manajemen_talenta/neun_box/load/unit-kerja/all";
                $('#kategori-unit-kerja').text('Unit Kerja : ' + param)

            }
            // alert(url)
            get_data(url, param)
        });


    }); // end am5.ready()
</script>

<!-- HTML -->
<div class="row mt-4">
    <div class="col-8">
        <div class="card">
            <div class="card-header">
                <h4>Nine Box</h4>
            </div>
            <div class="card-header">
                <h6 id='kategori-unit-kerja'>Unit Kerja : All</h6>
            </div>
            <div class="card-body">
                <div class="row">
                    <div id="chartdiv"></div>
                    <div class="col-md-12 buttons" id="filter_button">
                        <!-- <button class="btn btn-outline-primary" id="btn-300">50</button>
                        <button class="btn btn-outline-secondary" id="btn-200">40</button>
                        <button class="btn btn-outline-info" id="btn-100">33</button> -->
                    </div>
                </div>
                <div class="row mt-4">

                    <h5>Filter</h5>
                    <hr>
                    <div class="form-group col-md-6">
                        <label>Unit Kerja</label>
                        <select id='filter-unit-kerja' class="form-control">
                            <option value='all'>All</option>
                            <option value="Sekretariat Direktorat Jenderal Pengembangan Ekspor Nasional">Sekretariat Direktorat Jenderal Pengembangan Ekspor Nasional</option>
                            <option value="Direktorat Kerja Sama Pengembangan Ekspor">Direktorat Kerja Sama Pengembangan Ekspor</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Jabatan</label>
                        <div class="" id="filter_category">
                            <!-- <div class="pretty p-default">
                                    <input type="checkbox" id='data_1' checked>
                                    <div class="state p-success">
                                        <label>Ahli Pertama</label>
                                    </div>
                                </div> -->

                        </div>
                        <div class="">
                            <div class="pretty p-default">
                                <input type="checkbox" id='select_all' checked>
                                <div class="state p-success">
                                    <label>Select All</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">

            </div>
        </div>
    </div>


    <div class="col-4" id="detal_all_data">
        <div class="card">
            <div class="card-header">
                <h4>Detail Pegawai</h4>
            </div>
            <div class="card-body author-box">
                <div class="form-group">
                    <label>Cari Pegawai</label>
                    <form id="form_cari_pegawai_nine_box">
                        <div class="input-group colorpickerinput colorpicker-element" data-colorpicker-id="2">
                            <input type="text" class="form-control" id="cari_pegawai">
                            <div class="input-group-append">
                                <button class="input-group-text" id="submit-search" type='submit'>
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="" id="list-detail-data">
                    <div class="">
                        <ul class="nav nav-tabs" id="myTab2" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab2" data-bs-toggle="tab" href="#by_box" role="tab" aria-selected="true">By Box</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab2" data-bs-toggle="tab" href="#by_jabatan" role="tab" aria-selected="false">By Jabatan</a>
                            </li>
                        </ul>
                        <div class="tab-content tab-bordered" id="myTab3Content">
                            <div class="tab-pane fade show active" id="by_box" role="tabpanel" aria-labelledby="home-tab2">

                                <table class="table" id='tabel-list-by-box'>
                                    <thead>
                                        <tr>
                                            <th scope="col">Kategori</th>
                                            <th scope="col" class="text-center">Jumlah</th>
                                            <th scope="col" class="text-center">Presentase</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr onclick="show_tabel_by_box(0)">
                                            <td scope="col">Box 1</td>
                                            <td scope="col" class="text-center" id="box1">0</td>
                                            <td scope="col" class="text-center">0</td>
                                        </tr>
                                        <tr onclick="show_tabel_by_box(1)">
                                            <td scope="col">Box 2</td>
                                            <td scope="col" class="text-center" id="box2">0</td>
                                            <td scope="col" class="text-center">0</td>
                                        </tr>
                                        <tr onclick="show_tabel_by_box(2)">
                                            <td scope="col">Box 3</td>
                                            <td scope="col" class="text-center" id="box3">0</td>
                                            <td scope="col" class="text-center">0</td>
                                        </tr>
                                        <tr onclick="show_tabel_by_box(3)">
                                            <td scope="col">Box 4</td>
                                            <td scope="col" class="text-center" id="box4">0</td>
                                            <td scope="col" class="text-center">0</td>
                                        </tr>
                                        <tr onclick="show_tabel_by_box(4)">
                                            <td scope="col">Box 5</td>
                                            <td scope="col" class="text-center" id="box5">0</td>
                                            <td scope="col" class="text-center">0</td>
                                        </tr>
                                        <tr onclick="show_tabel_by_box(5)">
                                            <td scope="col">Box 6</td>
                                            <td scope="col" class="text-center" id="box6">0</td>
                                            <td scope="col" class="text-center">0</td>
                                        </tr>
                                        <tr onclick="show_tabel_by_box(6)">
                                            <td scope="col">Box 7</td>
                                            <td scope="col" class="text-center" id="box7">0</td>
                                            <td scope="col" class="text-center">0</td>
                                        </tr>
                                        <tr onclick="show_tabel_by_box(7)">
                                            <td scope="col">Box 8</td>
                                            <td scope="col" class="text-center" id="box8">0</td>
                                            <td scope="col" class="text-center">0</td>
                                        </tr>
                                        <tr onclick="show_tabel_by_box(8)">
                                            <td scope="col">Box 9</td>
                                            <td scope="col" class="text-center" id="box9">0</td>
                                            <td scope="col" class="text-center">0</td>
                                        </tr>
                                        <tr>
                                            <td scope="col"><b>Total</b></td>
                                            <td scope="col" c class="text-center"><b id="total_box">0</b></td>
                                            <td scope="col" c class="text-center"><b>100</b></td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                            <div class="tab-pane fade" id="by_jabatan" role="tabpanel" aria-labelledby="profile-tab2">

                                <table class="table" id='tabel-list-by-jabatan'>
                                    <thead>
                                        <tr>
                                            <th scope="col">Kategori</th>
                                            <th scope="col" class="text-center">Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="card-body author-box" id="personal_detail">
                </div>

            </div>
            <div class="card-footer">
                <button class="btn btn-primary" id="show_all">Show All</button>
            </div>
        </div>
    </div>
</div>
<div class="row mt-4" id="hasil_uji_kompetensi">
    <div class="col-12 col-md-12 col-lg-4">
        <div class="card">
            <div class="card-header">
                <h4>Detail Data</h4>
            </div>
            <div class="card-body author-box">
                <div class="card-body author-box" id="personal_detail_bc">
                    <div class="author-box-center ">
                        <!-- <img alt="image"
                            src="https://puskesmassentani.jayapurakab.go.id/wp-content/uploads/2020/07/profil-male-300x300.jpg"
                            class="rounded-circle author-box-picture" id="img-profile"> -->
                        <img alt="image" src="" class="rounded-circle author-box-picture" id="img-profile">
                        <div class="clearfix"></div>
                        <div class="author-box-name ">
                            <a href="#" id="nama">Nama</a>
                        </div>
                        <div class="author-box-job" id="pangkat">Pangkat</div>
                        <div class="author-box-job" id="categori_box">Box</div>

                    </div>
                    <div class="py-4">
                        <p class="clearfix">
                            <span class="float-left section-title">
                                NIP
                            </span>
                        </p>
                        <p class="clearfix">
                            <span class="float-left text-muted" id="nip">
                                NIP
                            </span>
                        </p>
                        <p class="clearfix">
                            <span class="float-left section-title">
                                Jabatan
                            </span>

                        </p>
                        <p class="clearfix">

                            <span class="float-left text-muted" id="jabatan">
                                Jabatan
                            </span>
                        </p>
                        <p class="clearfix">

                            <span class="float-left section-title">
                                Unit Kerja
                            </span>
                        </p>
                        <p class="clearfix">
                            <span class="pull-left text-muted" id="unit_kerja">
                                Unit Kerja
                            </span>
                        </p>
                        <p class="clearfix">
                            <span class="float-left section-title">
                                Pendidikan
                            </span>
                        </p>
                        <p class="clearfix">
                            <span class="float-left text-muted" id="pendidikan">
                                Pendidikan
                            </span>
                        </p>

                    </div>
                </div>
                <div class="card-footer empty-state" id="link-download">
                    <a href='#'>Download Assessment</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-12 col-lg-4">
        <div class="card">
            <div class="card-header">
                <h4>Detail Penilaian Sumbu X</h4>
            </div>
            <div class="card-body">
                <table class="table" id='tabel-sumbu-x'>
                    <thead>
                        <tr>
                            <th scope="col" class="">Kategori Penilaian</th>
                            <th scope="col" class="text-center">Nilai</th>
                            <th scope="col" class="text-center">Maksimal</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td scope="col">Kompetensi *</td>
                            <td scope="col" class="text-center" id="x_kompetensi_45">0</td>
                            <td scope="col" class="text-center" id='x_kompetensi'>45</td>
                        </tr>

                        <tr>
                            <td scope="col"><a style='color:red;'>Potensi <a></td>
                            <td scope="col" class="text-center" id="x_potensi_30">0</td>
                            <td scope="col" class="text-center" id="x_potensi">35</td>
                        </tr>

                        <tr>
                            <td scope="col">Diklat *</td>
                            <td scope="col" class="text-center" id="x_diklat_3">0</td>
                            <td scope="col" class="text-center" id='x_diklat'>2</td>
                        </tr>

                        <tr>
                            <td scope="col">Pendidikan *</td>
                            <td scope="col" class="text-center" id="x_pendidikan_9">0</td>
                            <td scope="col" class="text-center" id='x_pendidikan'>5</td>
                        </tr>
                        <tr>
                            <td scope="col"><a style='color:red;'>Penugasan <a></td>
                            <td scope="col" class="text-center" id="x_penugasan_8">0</td>
                            <td scope="col" class="text-center" id='x_penugasan'>10</td>
                        </tr>
                        <tr>
                            <td scope="col">Bahasa *</td>
                            <td scope="col" class="text-center" id="x_bahasa_5">0</td>
                            <td scope="col" class="text-center" id='x_bahasa'>3</td>
                        </tr>
                        <tr>

                            <td scope="col"><b>Total </b></td>
                            <td scope="col" class="text-center"><b id="xaxis100">0</b></td>
                            <td scope="col" class="text-center">100</td>
                        </tr>
                    </tbody>
                </table>
                * double click for detail
            </div>
        </div>
    </div>

    <div class="col-12 col-md-12 col-lg-4">
        <div class="card">
            <div class="card-header">
                <h4>Detail Penilaian Sumbu Y</h4>
            </div>
            <div class="card-body">
                <table class="table" id='tabel-sumbu-y'>
                    <thead>
                        <tr>
                            <th scope="col" class="">Kategori Penilaian</th>
                            <th scope="col" class="text-center">Nilai</th>
                            <th scope="col" class="text-center">Maksimal</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td scope="col">Sasaran Kerja Pegawai *</td>
                            <td scope="col" class="text-center" id="y_skp_15">0</td>
                            <td scope="col" class="text-center" id='y_skp'>15</td>
                        </tr>

                        <tr>
                            <td scope="col">Vote *</td>
                            <td scope="col" class="text-center" id="y_vote_50">0</td>
                            <td scope="col" class="text-center" id='y_vote'>50</td>
                        </tr>

                        <tr>
                            <td scope="col">EOTM *</td>
                            <td scope="col" class="text-center" id="y_eotm_15">0</td>
                            <td scope="col" class="text-center" id='y_eotm'>15</td>
                        </tr>

                        <tr>
                            <td scope="col">Absensi </td>
                            <td scope="col" class="text-center" id="y_absensi_5">0</td>
                            <td scope="col" class="text-center" id='y_absensi'>5</td>
                        </tr>

                        <tr>
                            <td scope="col" style="color:red;">Inovasi </td>
                            <td scope="col" class="text-center" id="y_inovasi_15">0</td>
                            <td scope="col" class="text-center" id='y_inovasi'>15</td>
                        </tr>

                        <tr>
                            <td scope="col"><b>Total </b></td>
                            <td scope="col" class="text-center"><b id="yaxis100">0</b></td>
                            <td scope="col" class="text-center">100</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row mt-sm-4">

</div>
<div class="card">
    <div class="card-header" id="all_data">
        <div class="col-md-6">
            <h4>Data Pegawai</h4>
        </div>
        <div class="col-md-6 pull-right" style="text-align:right" id="judul_tabel">
            All Series
        </div>

    </div>
    <div class="card-body">
        <table class="table table-striped" id='table-detail'>
            <thead>
                <tr>
                    <th>Nama</th>
                    <th>x value</th>
                    <th>y value</th>
                    <th>categori box</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <a>
                            <span class="d-inline-block ml-1"></span>
                        </a>

                    </td>
                    <td>
                        <p style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;max-width: 20ch;">
                            a
                        </p>

                    </td>
                    <td>
                        a
                    </td>
                    <td>
                        a
                    </td>

                </tr>


            </tbody>
        </table>
    </div>
</div>