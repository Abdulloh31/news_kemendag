<div class="modal fade bd-example-modal-xl" tabindex="-1" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true" id='modal-view-detail'>
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myLargeModalLabel">Detail Nilai</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-5">
                        <div class="card">
                            <div class="card-header">
                                <h4>Pemetaan</h4>
                            </div>
                            <div class="card-body author-box">

                                <table class="table" id='tabel-pemetaan'>
                                    <thead>
                                        <tr>
                                            <th scope="col">Komponen</th>
                                            <th scope="col" class="text-center">Standart Kompetensi</th>
                                            <th scope="col" class="text-center">Hasil Asesmen</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="card">
                            <div class="card-header">
                                <h4>Grafik Pemetaan</h4>
                            </div>
                            <div class="card-body">
                                <div class="mb-4">

                                    <div class="col-md-12">
                                        <h5 id="name_on_grap"></h5>
                                    </div>
                                    <div class="col-md-3">
                                        <select class="form-control " id="change_level">
                                            <option value="Pemetaan">Pemetaan</option>
                                            <option value="Promosi">Promosi</option>
                                        </select>
                                    </div>

                                    <div class="text-small float-right fw-bold text-muted" id=''></div>

                                </div>
                                <div class="mb-4">
                                    <div id="chart_radar"> </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="text-small float-right fw-bold text-muted" id='total'></div>
                                <div class="fw-bold">Total</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- modal diklat -->
<div class="modal fade bd-example-modal-xl" tabindex="-1" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true" id='modal-view-detail-diklat'>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myLargeModalLabel">Detail Nilai</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Diklat / Pelatihan</h4>
                            </div>
                            <div class="card-body author-box">
                                <table class="table table-striped" id='tabel-diklat'>
                                    <thead>
                                        <tr>
                                            <th>Nama</th>
                                            <th>Instansi</th>
                                            <th>Tanggal Ijazah</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <a>
                                                    <span class="d-inline-block ml-1"></span>
                                                </a>

                                            </td>
                                            <td>
                                                <p style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;max-width: 20ch;">
                                                    a
                                                </p>

                                            </td>
                                            <td>
                                                a
                                            </td>


                                        </tr>


                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- modal pendidikan -->
<div class="modal fade bd-example-modal-xl" tabindex="-1" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true" id='modal-view-detail-pendidikan'>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myLargeModalLabel">Detail Nilai</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Pendidikan</h4>
                            </div>
                            <div class="card-body author-box">
                                <table class="table table-striped" id='tabel-pendidikan'>
                                    <thead>
                                        <tr>
                                            <th>Tingkat Pendidikan</th>
                                            <th>Nama Sekolah</th>
                                            <th>Tanggal Ijazah</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <a>
                                                    <span class="d-inline-block ml-1"></span>
                                                </a>

                                            </td>
                                            <td>
                                                <p style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;max-width: 20ch;">
                                                    a
                                                </p>

                                            </td>
                                            <td>
                                                a
                                            </td>


                                        </tr>


                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- modal bahasa -->
<div class="modal fade bd-example-modal-xl" tabindex="-1" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true" id='modal-view-detail-bahasa'>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myLargeModalLabel">Detail Nilai</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Bahasa</h4>
                            </div>
                            <div class="card-body author-box">
                                <table class="table table-striped" id='tabel-bahasa'>
                                    <thead>
                                        <tr>
                                            <th>Jenis Kemampuan Bahasa</th>
                                            <th>Instansi</th>
                                            <th>Tanggal Sertifikat</th>
                                            <th>Skor</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><a><span class="d-inline-block ml-1"></span></a></td>
                                            <td>a</td>
                                            <td>a</td>
                                            <td>a</td>


                                        </tr>


                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal vote -->
<div class="modal fade bd-example-modal-xl" tabindex="-1" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true" id='modal-view-detail-vote'>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myLargeModalLabel">Detail Nilai</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="text-center">Vote Pegawai Terbaik</h4>
                            </div>
                            <div class="card-body author-box">
                                <div id="chart_line_vote"></div>
                                <hr>


                            </div>
                            <div class="card-body">
                                <div class="col-md-12">
                                    Nilai Saat ini
                                </div>
                                <div class='col-md-12'>
                                    <table class="table table-striped text-center" id='tabel-vote'>
                                        <thead>
                                            <tr>
                                                <th>Peer Review (60%)</th>
                                                <th>Vote Terbaik (40%)</th>
                                                <th>Key Player</th>
                                                <th>Nilai Akhir</th>
                                                <th>Nilai Nine Box</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>76,74</td>
                                                <td>89,12</td>
                                                <td>10,00</td>
                                                <td>91,69</td>
                                                <td>x</td>
                                            </tr>


                                        </tbody>
                                    </table>
                                </div>

                                <div class="col-md-12">
                                    <code style="color:black">Nilai Nine Box = Nilai akhir/110 x 50</code>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- modal eotm -->
<div class="modal fade bd-example-modal-xl" tabindex="-1" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true" id='modal-view-detail-eotm'>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myLargeModalLabel">Peringkat Pegawai Terbaik</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <div class="card author-box">
                            <div class="card-body">
                                <div class="author-box-center">
                                    <img alt="image" src="http://paspeg.kemendag.go.id/assets/photo/1965/196507221992031002/Thumb_square_196507221992031002.jpg" class="rounded-circle author-box-picture" id="img-profile-2">
                                    <div class="clearfix"></div>
                                    <div class="author-box-name">
                                        <a id='nama-eotm'>Sarah Smith</a>
                                    </div>
                                    <div class="author-box-job" id="nip-eotm">NIP 129381023801</div>
                                </div>
                                <div class="text-center">

                                    <div class="mb-2 mt-3">
                                        <table class="table table-striped text-center" id='tabel-eotm'>
                                            <thead>
                                                <tr>
                                                    <th>Peer Review</th>
                                                    <th>Vote Terbaik</th>
                                                    <th>Key Player</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td id='peer-review-eotm'>76,74</td>
                                                    <td id='vote-eotm'>89,12</td>
                                                    <td id='key-player-eotm'>10,00</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3">
                                                        Peringkat Saat Ini<br>
                                                        <a id='peringkat-eotm'>-</a>
                                                    </td>
                                                </tr>


                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="w-100 d-sm-none"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3"></div>
                    <div class='col-md-12'>
                        <div class="card">
                            <div class="card-header">
                                <!-- <h4>Peringkat Pegawai Terbaik</h4> -->
                            </div>

                            <div class="card-body author-box">
                                <div id="chart_line_eotm"></div>
                                <hr>


                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- modal abensi  -->
<div class="modal fade bd-example-modal-xl" tabindex="-1" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true" id='modal-view-detail-absensi'>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myLargeModalLabel">Data Rekap Absensi <?= date('Y') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class='col-md-12'>
                        <div class="card">
                            <div class="card-header">
                                <!-- <h4>Peringkat Pegawai Terbaik</h4> -->
                            </div>

                            <div class="card-body author-box">
                                <div id="chart_bar_absensi"></div>
                                <hr>


                            </div>

                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6"></div>
                    <div class="col-xl-6 col-lg-6">
                        <div class="card l-bg-green-dark" id='akumulasi_card'>
                            <div class="card-statistic-3">
                                <div class="card-icon card-icon-large"><i class="fa fa-hourglass-half"></i></div>
                                <div class="card-content">
                                    
                                    <div>
                                        
                                    <h2 class="card-title text-center" id='nominal-akumulasi'>00:00</h2>
                                    </div>
                                    <p class="mb-0 text-sm text-center">
                                        <span class="mr-2">Akumulasi Terlambat &amp; tidak hadir</span>

                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3 col-lg-6"></div>
                </div>

            </div>
        </div>
    </div>
</div>



<!-- modal skp  -->
<div class="modal fade bd-example-modal-xl" tabindex="-1" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true" id='modal-view-detail-skp'>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myLargeModalLabel">Detail Nilai SKP <?= date('Y') ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class='col-md-12'>
                        <div class="card">
                            <div class="card-header">
                                <!-- <h4>Peringkat Pegawai Terbaik</h4> -->
                            </div>

                            <div class="card-body author-box">
                                <div id="chart_bar_skp"></div>
                                <hr>


                            </div>

                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>