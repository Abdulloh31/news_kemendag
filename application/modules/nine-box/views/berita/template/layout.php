<!DOCTYPE html>
<html lang="en">


<?php include 'header.php';?>



<body class='light theme-white light-sidebar'>
    <div class="loader"></div>
    <div id="app">
        <header>
            <div class="container mt-5">
                <div class="row">
                    <div class="col-12 col-md-12 ">
                        <div class="card">
                            <div class="card-header">

                            </div>
                            <div class="card-body">
                                <nav class="navbar navbar-expand-lg bg-primary">
                                    <a class="navbar-brand" href="#">Kementerian Perdagangan</a>
                                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
                                        aria-label="Toggle navigation">
                                        <span class="navbar-toggler-icon"></span>
                                    </button>
                                    <div class="collapse navbar-collapse navbar-right" id="navbarNav">
                                        <ul class="navbar-nav">
                                            <li class="nav-item <?= ($title == 'Home')?'active':'' ?>">
                                                <a class="nav-link" href="<?php  echo base_url(); ?>index.php/News/Home">Home <span
                                                        class="sr-only">(current)</span></a>
                                            </li>
                                            <li class="nav-item <?= ($title == 'News Data')?'active':'' ?>">
                                                <a class="nav-link" href="<?php  echo base_url(); ?>index.php/News/News">Data News</a>
                                            </li>
                                            <li class="nav-item <?= ($title == 'Data Category')?'active':'' ?>">
                                                <a class="nav-link" href="<?php  echo base_url(); ?>index.php/News/Category">Data Category</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Logout</a>
                                            </li>

                                        </ul>
                                    </div>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </header>
        <section class="section">
            <div class="container mt-5">
                <?php echo $contents ?>
            </div>
        </section>
    </div>

</body>
<?php include 'scripts.php';?>

</html>