<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>Kementerian Perdagangan - <?php echo $title ?></title>
    <!-- General CSS Files -->

    <link rel="stylesheet" href="<?php  echo base_url(); ?>assets/news/css/app.min.css">
    <link rel="stylesheet" href="<?php  echo base_url(); ?>assets/news/bundles/jquery-selectric/selectric.css">
    <!-- Template CSS -->
    <link rel="stylesheet" href="<?php  echo base_url(); ?>assets/news/css/style.css">
    <link rel="stylesheet" href="<?php  echo base_url(); ?>assets/news/css/components.css">

    <link rel="stylesheet" href="<?php  echo base_url(); ?>assets/news/bundles/pretty-checkbox/pretty-checkbox.min.css">
    <!-- Custom style CSS -->
    <link rel="stylesheet" href="<?php  echo base_url(); ?>assets/news/css/custom.css">
    <link rel='shortcut icon' type='image/x-icon' href='<?php  echo base_url(); ?>assets/news/img/favicon.ico' />
    
    <link rel="stylesheet" href="<?php  echo base_url(); ?>assets/news/bundles/select2/dist/css/select2.min.css">

    <!-- AJAX -->
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script> -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>


    <!-- UNtuk datatable -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">



    <!-- //summnernote -->
    <!-- include summernote css/js -->
    <!-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet"> -->


    <!-- by aegis -->
    <link rel="stylesheet" href="<?php  echo base_url(); ?>assets/news/bundles/summernote/summernote-bs4.css">
    <link rel="stylesheet" href="<?php  echo base_url(); ?>assets/news/bundles/codemirror/lib/codemirror.css">
    <link rel="stylesheet" href="<?php  echo base_url(); ?>assets/news/bundles/codemirror/theme/duotone-dark.css">
    <link rel="stylesheet" href="<?php  echo base_url(); ?>assets/news/bundles/jquery-selectric/selectric.css">




</head>