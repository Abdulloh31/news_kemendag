  <!-- General JS Scripts -->
  <script src="<?php  echo base_url(); ?>assets/news/js/app.min.js"></script>
  <!-- JS Libraies -->
  <script src="<?php  echo base_url(); ?>assets/news/bundles/jquery-selectric/jquery.selectric.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="<?php  echo base_url(); ?>assets/news/js/page/posts.js"></script>
  
  <!-- Template JS File -->
  <script src="<?php  echo base_url(); ?>assets/news/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="<?php  echo base_url(); ?>assets/news/js/custom.js"></script>

  <!-- Data Table -->
  <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap4.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>
  <script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>

  <!-- Summer Note -->
  <!-- JS Libraies -->
  <script src="<?php  echo base_url(); ?>assets/news/bundles/summernote/summernote-bs4.js"></script>
  <script src="<?php  echo base_url(); ?>assets/news/bundles/codemirror/lib/codemirror.js"></script>
  <script src="<?php  echo base_url(); ?>assets/news/bundles/codemirror/mode/javascript/javascript.js"></script>
  <script src="<?php  echo base_url(); ?>assets/news/bundles/jquery-selectric/jquery.selectric.min.js"></script>

  
  <script src="<?php  echo base_url(); ?>assets/news/bundles/select2/dist/js/select2.full.min.js"></script>
  <!-- <script src="<?php  echo base_url(); ?>assets/news/bundles/ckeditor/ckeditor.js"></script> -->
  <!-- Page Specific JS File -->
  <!-- <script src="<?php  echo base_url(); ?>assets/news/js/page/ckeditor.js"></script> -->
  <script src="<?php  echo base_url(); ?>assets/news/bundles/summernote/summernote-bs4.js"></script>



  <!-- Summernote boostrap -->
  <!-- include summernote css/js -->
  <!-- <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script> -->