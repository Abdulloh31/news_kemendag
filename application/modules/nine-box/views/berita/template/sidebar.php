<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="index.html"> <img alt="image" src="<?php  echo base_url(); ?>assets/news/img/logo.png"
                    class="header-logo" /> <span class="logo-name">Aegis</span>
            </a>
        </div>
        <div class="sidebar-user">
            <div class="sidebar-user-picture">
                <img alt="image" src="<?php  echo base_url(); ?>assets/news/img/userbig.png">
            </div>
            <div class="sidebar-user-details">
                <div class="user-name">Sarah Smith</div>
                <div class="user-role">Administrator</div>
            </div>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Main</li>
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown"><i data-feather="monitor"></i><span>Dashboard</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="index.html">Dashboard V1</a></li>
                    <li><a class="nav-link" href="index2.html">Dashboard V2</a></li>
                </ul>
            </li>
        </ul>
    </aside>
</div>