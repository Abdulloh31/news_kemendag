<?php

class Category extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */
	public function index()
	{
		// $query = $this->db->query('SELECT * FROM t_user');
        $data['title'] =  "Data Category";
        $data['category'] = $this->db->get('t_category');

        //counting
        //draft
      

		$this->template->load('berita/template/layout','berita/category/index',$data);
        $this->load->view('berita/category/modal');
	}

	public function test(){
		$response['success'] = true;
		$response['messages'] = "Success get data.";
        echo json_encode($response);
	}

	public function getDataCategory(){
        
        if ($this->input->is_ajax_request()) {
            $response['success'] = true;
            $response['messages'] = "Success get data.";
            //data post
			$this->db->select('*');
			$this->db->from('t_category');
			$this->db->where('id',$_POST['id']);
			$data = $this->db->get()->result_array();
            $response['data'] = $data;
            
            
        }else{
            
            $response['success'] = false;
            $response['messages'] = "Access Denied.";
            
        }
        echo json_encode($response);
    } 


    public function store(){

        try{
            
            $now = date('Y-m-d h:i:sa');
            $data = array(
                'name' => $_POST['name'],
                'description'=> $_POST['description']
            );
            $this->db->insert('t_category', $data);
            $id_post =$this->db->insert_id();


            redirect(base_url()."index.php/News/Category");
        
        } catch (\Exception $exception){
				session()->setFlashdata('err', 'Create News Failed [error:'. $exception.']');
                redirect(base_url()."index.php/News");
        }
        
        
       
    }

	
    public function delete(){
        try{
            
            $id= $_POST['data'];
            $this->db->trans_begin();

			if($this->cekUsed($id)){
				$this->db->where('id', $id);
				$this->db->delete('t_category');
			}else{
				
                $this->session->set_flashdata('err', 'Data Category already used ');
                redirect(base_url()."index.php/News/Category");
			}
            
            
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                $this->session->set_flashdata('err', 'Failed data post ');
                redirect(base_url()."index.php/News/Category");
            }else{
                $this->db->trans_commit();
                $this->session->set_flashdata('err', 'Delete data post successfully');
                redirect(base_url()."index.php/News/Category");
            }
            
        } catch (\Exception $exception){
            session()->setFlashdata('err', 'Delete News Failed [error:'. $exception.']');
            redirect(base_url()."index.php/News/Category");
        }
    }

	
	private function cekUsed($id){
        $data = $this->db->query("SELECT * FROM t_post WHERE categori = '$id' ")->result_array();

		if(empty($data)){
			return true;
		}else{
			return false;
		}
	}

	
    public function update(){
        try{
            $id =$_POST['id_category'];

            // var_dump($id_post);
			$data = array(
                'name' => $_POST['name'],
                'description'=> $_POST['description']
            );
			print_r($id);
            //update post
            $this->db->where('id',$id);
            $this->db->update('t_category', $data);
    
            $this->session->set_flashdata('msg', 'Update data category successfully');
            redirect(base_url()."index.php/News/Category");
        } catch (\Exception $exception){
				session()->setFlashdata('err', 'Create Category Failed [error:'. $exception.']');
                redirect(base_url()."index.php/News/Category");
        }
        
    }
	

	  
}