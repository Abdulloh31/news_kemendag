<?php

class News extends CI_Controller {


	public function index()
	{
		// $query = $this->db->query('SELECT * FROM t_user');
        $this->db->select('*');
        $this->db->from('t_user');
        $query = $this->db->get();
        $data['user'] =  $query;
        $data['title'] =  "News Data";
        $data['post'] = $this->db->query("SELECT t_post.*,t_category.name AS category_name FROM t_post left join  t_category  on t_category.id = t_post.categori ORDER BY create_at DESC; ")->result_array();
        $data['image'] = $this->db->get('t_image');
        $data['category'] = $this->db->get('t_category');

        //counting
        //draft
        $draft = $this->db->query("SELECT COUNT(judul) as jumlah FROM t_post WHERE status = '1' ")->result_array();
        $pending = $this->db->query("SELECT COUNT(judul) as jumlah FROM t_post WHERE status = '2' ")->result_array();
        $publish = $this->db->query("SELECT COUNT(judul) as jumlah FROM t_post WHERE status = '3' ")->result_array();
        
        $data['all'] = count($data['post']);
        $data['draft'] =$draft[0]['jumlah'];
        $data['pending'] = $pending[0]['jumlah'];
        $data['publish'] = $publish[0]['jumlah'];

        // var_dump($data['all']);
        // $data['modal'] = $this->load->view('news/data_post/input');
        // $row = $query->row_array();
        // $this->load->view('news/content/index',$data);

		$this->template->load('berita/template/layout','berita/data_post/index',$data);
        $this->load->view('berita/data_post/input');
	}


    public function store(){

        try{
            //untuk validasi
            // $this->load->helper(array('form', 'url'));
            $this->load->library('form_validation');
            //set rule
            
            $this->form_validation->set_rules('judul', 'Judul', 'required|is_unique[t_post.judul]');
            $this->form_validation->set_rules('categori', 'Category', 'required');
            $this->form_validation->set_rules('konten', 'Content', 'required');
            $this->form_validation->set_rules('status', 'Status', 'required');
            $this->form_validation->set_rules('tags', 'Tags', 'required');
            $this->form_validation->set_rules('publish_at', 'Publish Date', 'required');
            $this->form_validation->set_rules('slug', 'Slug', 'required|is_unique[t_post.slug]');


            if ($this->form_validation->run() == FALSE){
                
                $this->session->set_flashdata('validation_input', 'Error Validation');
                $this->index();
                // redirect(base_url()."index.php/News");
            }else{
                
                $this->db->trans_begin();
                $now = date('Y-m-d h:i:sa');
                $slug = url_title($_POST['judul'], 'dash', true);
                $data = array(
                    'judul' => $_POST['judul'],
                    'categori'=> $_POST['categori'],
                    'konten'=> $_POST['konten'],
                    'status'=> $_POST['status'],
                    'tags' => $_POST['tags'],
                    'create_at'=> $now,
                    'publish_at'=> $_POST['publish_at'],
                    'views' =>'0',
                    'author' =>'nip',
                    'slug' =>$_POST['slug']
                );
                $this->db->insert('t_post', $data);
                $id_post =$this->db->insert_id();

                $this->db->trans_commit();

                
                $this->load->library('upload');
                $dataInfo = array();
                $files = $_FILES;
                if( !empty($_FILES['image']['name'][0]) && $_FILES['image']['name'][0] != ""){
                    $cpt = count($_FILES['image']['name']);
                }else{
                    $cpt =0;
                }
                if($cpt > 0){
                    for($i=0; $i<$cpt; $i++){         
                        $tempfile = $files['image']['tmp_name'][$i];
                        $image = base64_encode(file_get_contents($tempfile)); 
                        if($tempfile != ""){
                            $image = base64_encode(file_get_contents($tempfile)); 
                            
                            $data_image = array(
                                'id_post'=> $id_post,
                                'file_image'=> $image,
                            );
                            
                            $this->db->insert('t_image', $data_image);
                        }else{
                            $image ="";
                        }
                        // $this->upload->initialize($this->set_upload_options());
                        // $this->upload->do_upload('userfile');
                        // $dataInfo[] = $this->upload->data();
                    }
                }
                if ($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();
                    $this->session->set_flashdata('err', 'Failed data post ');
                    redirect(base_url()."index.php/News");
                }else{
                    $this->db->trans_commit();
                    $this->session->set_flashdata('msg', 'Create Post Successfully');
                    redirect(base_url()."index.php/News");
                }
                //return views

                // redirect(base_url()."index.php/News");
            }
            

        
        } catch (\Exception $exception){
				session()->setFlashdata('err', 'Create News Failed [error:'. $exception.']');
                redirect(base_url()."index.php/News");
        }
        
        
       
    }


    public function getDataPost(){
        
        if ($this->input->is_ajax_request()) {
            $response['success'] = true;
            $response['messages'] = "Success get data.";
            //data post
			$this->db->select('*');
			$this->db->from('t_post');
			$this->db->where('id',$_POST['id']);
			$data = $this->db->get()->result_array();
            $response['data'] = $data;
            //data image
			$this->db->select('*');
			$this->db->from('t_image');
			$this->db->where('id_post',$_POST['id']);
			$data_image = $this->db->get()->result_array();
            $response['data_image'] = $data_image;
            
        }else{
            
            $response['success'] = false;
            $response['messages'] = "Access Denied.";
            
        }
        echo json_encode($response);
    } 

    public function update(){
        try{
            $id_post =$_POST['id_post'];

            $data = array(
                'judul' => $_POST['judul'],
                'slug' => $_POST['slug'],
                'categori'=> $_POST['category'],
                'konten'=> $_POST['content'],
                'status'=> $_POST['status'],
                'tags' => $_POST['tags'],
                'publish_at'=> $_POST['publish_at'],
            );

            //untuk validasi
            // $this->load->helper(array('form', 'url'));
            $this->load->library('form_validation');
            //set rule
            
            $this->form_validation->set_rules('judul', 'Judul', 'required|is_unique[t_post.judul]');
            $this->form_validation->set_rules('categori', 'Category', 'required');
            $this->form_validation->set_rules('konten', 'Content', 'required');
            $this->form_validation->set_rules('status', 'Status', 'required');
            $this->form_validation->set_rules('tags', 'Tags', 'required');
            $this->form_validation->set_rules('publish_at', 'Publish Date', 'required');
            $this->form_validation->set_rules('slug', 'Slug', 'required|is_unique[t_post.slug]');


            if ($this->form_validation->run() == FALSE){
                
                $this->session->set_flashdata('validation_edit', '1');
                $this->index();
                // redirect(base_url()."index.php/News");
            }else{
                //update post
                $this->db->where('id',$id_post);
                $this->db->update('t_post', $data);

                // update image
                $this->load->library('upload');
                $files = $_FILES;
                if( !empty($_FILES['image']['name'][0]) && $_FILES['image']['name'][0] != ""){
                    $cpt = count($_FILES['image']['name']);
                }else{
                    $cpt=0;
                }
                if($cpt > 0){
                    $this->db->where('id_post', $id_post);
                    $this->db->delete('t_image');
                    for($i=0; $i<$cpt; $i++){         
                        $tempfile = $files['image']['tmp_name'][$i];
                        $image = base64_encode(file_get_contents($tempfile)); 

                        // $this->upload->initialize($this->set_upload_options());
                        // $this->upload->do_upload('userfile');
                        // $dataInfo[] = $this->upload->data();
                        $data_image = array(
                            'id_post'=> $id_post,
                            'file_image'=> $image,
                        );
                        
                        $this->db->insert('t_image', $data_image);
                    }
                }
                $this->session->set_flashdata('msg', 'Update data post successfully');
                redirect(base_url()."index.php/News");
            }
            
        } catch (\Exception $exception){
                $this->session->set_flashdata('err', $exception->getMessage());
                redirect(base_url()."index.php/News");
        }
        
    }

    public function delete(){
        try{
            
            $id_post= $_POST['data'];
            $this->db->trans_begin();
        
            //delete image tabel
            
            $this->db->where('id_post', $id_post);
            $this->db->delete('t_image');

            //delete post tabel
            
            $this->db->where('id', $id_post);
            $this->db->delete('t_post');
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                $this->session->set_flashdata('err', 'Failed data post ');
                redirect(base_url()."index.php/News");
            }else{
                $this->db->trans_commit();
                $this->session->set_flashdata('msg', 'Delete data post successfully');
                redirect(base_url()."index.php/News");
            }
            
        } catch (\Exception $exception){
            session()->setFlashdata('err', 'Delete News Failed [error:'. $exception.']');
            redirect(base_url()."index.php/News");
        }
    }


    public function get_by_status($status){
        if ($this->input->is_ajax_request()) {
            $response['success'] = true;
            $response['messages'] = "Success get data.";
            //data post
            if($status == '0'){
                $this->db->select('*');
                $this->db->from('t_post');
                $data = $this->db->get()->result_array();
            }else{
                $this->db->select('*');
                $this->db->from('t_post');
                $this->db->where('status',$status);
                $data = $this->db->get()->result_array();
            }
            $response['data'] = $data;
            if(!empty($data)){
                foreach ($data as $key => $value) :
                    if($value['status'] == '1') : 
                         
                    elseif($value['status'] == '2'): 
                        $status = '<div class="badge btn-warning">Pending</div>';  
                    else: 
                        $status = '<div class="badge badge-primary">Published</div>';  
                    endif;
                    
                    $result['data'][$key] = array(
                        "<a>
                            <span class='d-inline-block ml-1'>".$value['judul']."</span>
                        </a>
                        <div class='table-links'>
                            <form action=".base_url('index.php/News/delete')." method='post'
                                enctype='multipart/form-data' id='form-input'>
                                <a type='button' data-id='".$value['id']."'
                                    class='btn-detail ' onclick='view(this)'>Detail</a>
                                <div class='bullet'></div>
                                <input type='hidden' value='". $value['id']."' name='data'>
                                <a type='submit' class='btn-delete' onclick='del(this)'>Trash</a>
                            </form>
                        </div>",
                        "<p style='white-space: nowrap;overflow: hidden;text-overflow: ellipsis;max-width: 20ch;'>".strip_tags($value['konten']) ."</p>",
                        $value['categori'],
                        date('d-M-Y',strtotime($value['create_at'])),
                        date('d-M-Y',strtotime($value['publish_at'])),
                        $value['tags'],
                        $status,
                    );
                endforeach;
            }else{
                $result['data'] = [];
            }

   
        }else{
            
            $result['success'] = false;
            $result['messages'] = "Access Denied.";
            
        }
        echo json_encode($result);
    }
}