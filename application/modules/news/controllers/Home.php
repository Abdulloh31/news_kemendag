<?php

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */
	public function index()
	{
		// $query = $this->db->query('SELECT * FROM t_user');
        
        $show_news = $this->db->query("SELECT TOP 1 *
		FROM t_post
		ORDER BY create_at DESC; ")->result_array();
        $query = $this->db->query("SELECT TOP 3 *
		FROM t_post
		where id <> '".$show_news[0]['id']."'
		ORDER BY create_at DESC; ")->result_array();
		$image = $this->db->query("SELECT  * FROM t_image;")->result_array();
		$data['image'] = $image;
        $data['new_post'] =  $query;
        $data['show_news'] =  $show_news;
        $data['title'] =  "Home";
        // $row = $query->row_array();
        // $this->load->view('news/content/index',$data);
		$this->template->load('berita/template/layout','berita/content/index',$data);
	}

	public function view($slug){
		// $id_post =  slug_decode($slug);
		$show_news = $this->db->query("SELECT  * FROM t_post  where slug = '$slug' ORDER BY create_at DESC; ")->result_array();
		if(empty($show_news)){
			redirect(base_url()."index.php/News/Home");
		}
        $new_post = $this->db->query("SELECT TOP 3 *
		FROM t_post
		where slug <> '$slug'
		ORDER BY create_at DESC; ")->result_array();
		$image = $this->db->query("SELECT  * FROM t_image;")->result_array();
		$data['image'] = $image;
        $data['new_post'] =  $new_post;
        $data['show_news'] =  $show_news;
        $data['title'] =  "Home";
        // $row = $query->row_array();
        // $this->load->view('news/content/index',$data);
		$this->template->load('berita/template/layout','berita/content/index',$data);

	}

	  
}