<div class="row">
    <div class="col-8 col-md-8 ">
        <div class="card author-box">
            <div class="card-body">

                <div class="">
                    <div class="row" id="div-image">
                        <!-- <img alt="image" src="<?= base_url() ?>assets/img/users/user-3.png"
                                    style="max-width: 100%; max-height: 100%;" id='view_image'> -->
                        <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                            <div class="carousel-inner">
                                <div id="show-image">
                                    <?php 
                                    $i = 0;
                                    foreach ($image as $k => $v){ 
                                    if($v['id_post'] == $show_news[0]['id']){     
                                    ?>
                                    <div class="carousel-item <?= ($i=='0')?'active':'' ?> ">
                                        <img class="rounded mx-auto d-block mw-100"
                                            src="data:image/jpg;charset=utf8;base64,<?= $v['file_image']?>"
                                            alt="First slide">
                                    </div>
                                    <?php 
                                    $i = $i+1;} 
                                    } ?>
                                </div>
                            </div>
                            <button class="carousel-control-prev" type="button"
                                data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Previous</span>
                            </button>
                            <button class="carousel-control-next" type="button"
                                data-bs-target="#carouselExampleControls" data-bs-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Next</span>
                            </button>
                        </div>
                    </div>
                    <div class="banner-content m-t-20">
                        <div class="badge badge-danger fs-12 font-weight-bold mb-3">
                            <?= $show_news[0]['tags'] ?>

                        </div>
                        <h1 class="mb-2"><?= $show_news[0]['judul'] ?></h1>
                        <h1 class="mb-2">

                        </h1>
                        <div class="fs-12" style="text-align: justify">
                            <?= $show_news[0]['konten'] ?>
                            <!-- <span class="mr-2">Photo </span>10 Minutes ago -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <h4>Lates News</h4>
            </div>
            <div class="card-body pb-0">

                <?php foreach ($new_post as $key => $value){ ?>

                <article class="article">
                    <div class="article-header">

                        <?php 
                        $i = 0;
                        foreach ($image as $k => $v){ 
                            if($v['id_post'] == $value['id'] && $i ==0){
                                $i = $i+1;
                        ?>

                        <div class="article-image " widht='1000px'
                            data-background="<?php  echo base_url(); ?>assets/news/img/blog/img08.png"
                            style='background-image: url("<?php  echo base_url(); ?>assets/news/img/blog/img04.png");'>
                            <img src="data:image/jpg;charset=utf8;base64,<?= $v['file_image']?>" alt="Girl in a jacket"
                                width="500" height="600" class="article-image ">
                        </div>
                        <?php }  } ?>

                        <div class="article-title">
                            <h2><a href="#"><?= $value['judul'] ?></a></h2>
                            <p style='color:white;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;max-width: 75ch;'><?= strip_tags($value['konten']) ?> </p>
                        </div>
                    </div>
                    <div class="article-details">

                        <div class="article-cta">
                            <a class="btn btn-primary" data-id="<?= $value['id']?>" href="<?= base_url('index.php/News/Home/view/'); ?><?= $value['slug']?>">Read More</a>
                        </div>
                    </div>
                </article>
                <?php } ?>



            </div>

        </div>
    </div>

</div>