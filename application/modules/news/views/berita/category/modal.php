<!-- Modal input -->

<form action="<?= base_url() ?>index.php/News/Category/store" method="post" enctype="multipart/form-data" id="form-input">
    <div class="modal fade bd-example-modal-md" tabindex="-1" aria-labelledby="myLargeModalLabel" style="display: none;"
        aria-hidden="true" id='modal-input'>
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myLargeModalLabel">Input Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="card-body">
                <div class="form-group">
                            <label>Category Name</label>
                            <input type="text" class="form-control form-control-sm" name='name' id='name_input' required>
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <input type="text" class="form-control form-control-sm" name='description' id='description_input' required>
                        </div>
                    <div class="form-group">

                        <button class="btn btn-primary mr-1" type="submit">Submit</button>
                    </div>


                </div>
            </div>
        </div>
    </div>

</form>

<!-- Modal View -->
<form method="post" class="needs-validation" action="<?= base_url()?>index.php/News/Category/update"
    enctype="multipart/form-data">

    <div class="modal fade bd-example-modal-md" tabindex="-1" aria-labelledby="myLargeModalLabel" style="display: none;"
        aria-hidden="true" id='modal-view'>
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myLargeModalLabel">Edit Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" class="form-control form-control-sm" name='id_category' id='id_category'
                        required>
                    <div class="card-body">
                        <div class="form-group">
                            <label>Category Name</label>
                            <input type="text" class="form-control form-control-sm" name='name' id='name' required>
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <input type="text" class="form-control form-control-sm" name='description' id='description' required>
                        </div>

                        <button class="btn btn-primary" type="submit">Save Changes</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</form>


<script>
var tablePost

function getdate(Tanggal) {

    var now = new Date(Tanggal);

    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today = now.getFullYear() + "-" + month + "-" + (day);
    return today
}

function loadTabel(status) {
    tablePost.clear().draw();
    tablePost.ajax.url("<?= base_url('index.php/News/get_by_status/') ?>" + status).load()


}

function edit(data) {
    id = $(data).data('id')

    $.ajax({
        method: 'POST',
        url: '<?= base_url() ?>index.php/News/Category/getDataCategory',
        data: {
            id: id
        },
        dataType: "json",
        success: function(response) {
            if (response.success == true) {
                $('#id_category').val(response.data[0].id)
                $('#name').val(response.data[0].name)
                $('#description').val(response.data[0].description)
                $('#modal-view').modal('toggle')

            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Failed',
                    text: 'Error finding data'
                })
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Error finding data'
            })
        }
    })
}

function del(data) {
    Swal.fire({
        title: 'Warning',
        text: "Are you sure you want to delete this post ?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!',
        focusCancel: true,
        allowOutsideClick: false
    }).then((result) => {
        if (result.isConfirmed) {
            submit_delete = $(data).parent()
            submit_delete.submit();
        }
    })
}
$(document).ready(function() {
    

    tablePost = $("#table-post").DataTable({
        orderCellsTop: true,
        fixedHeader: true,
        ordering: true,
        paging: true
    })
   
    $('#btn-add').on('click', function(e) {
        // $('#form-input').find("input[type=text], textarea").val("");
        $('#modal-input').modal('toggle')

    })



})
</script>