<?php $category = $category->result_array() ?>

<div class="row mt-4">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4>All Posts</h4>
            </div>
            <div class="card-body">
                <div class="float-left">
                    <div class="selectric-wrapper selectric-form-control selectric-selectric">
                        <button type="button" class="btn btn-primary" id="btn-add">Add Category</button>
                        <div class="selectric-items" tabindex="-1">
                            <div class="selectric-scroll">
                                <ul>
                                    <li data-index="0" class="selected">Action For Selected</li>
                                    <li data-index="1" class="">Move to Draft</li>
                                    <li data-index="2" class="">Move to Pending</li>
                                    <li data-index="3" class="last">Delete Permanently</li>
                                </ul>
                            </div>
                        </div><input class="selectric-input" tabindex="0">
                    </div>
                </div>

                <div class="clearfix mb-3"></div>
                
                <div class="table-responsive">
                    <table class="table table-striped" id='table-post'>
                        <thead>
                            <tr>
                                <th>Category Name</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php foreach ($category as $key => $value){ ?>
                            <tr>
                                <td>
                                    <a>
                                        <span class="d-inline-block ml-1"><?= $value['name'] ?></span>
                                    </a>
                                    <div class="table-links">
                                        <form action="<?= base_url() ?>index.php/News/Category/delete" method="post"
                                            enctype="multipart/form-data" id="form-input">
                                            <a type="button" data-id="<?= $value['id'] ?>" onclick="edit(this)"
                                                class="btn-detail ">Edit</a>
                                            <div class="bullet"></div>
                                            <input type="hidden" value="<?= $value['id'] ?>" name="data">
                                            <a type="submit" class="btn-delete" onclick="del(this)">Trash</a>
                                        </form>
                                    </div>
                                </td>
                                <td>
                                    <?= $value['description']?>
                                </td>
                               
                            </tr>

                            <?php } ?>


                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>