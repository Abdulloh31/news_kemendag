<!-- Modal input -->

<?php $category = $category->result_array() ?>
<form action="<?= base_url() ?>index.php/News/News/store" method="post" enctype="multipart/form-data" id="form-input">
    <div class="modal fade bd-example-modal-lg" tabindex="-1" aria-labelledby="myLargeModalLabel" style="display: none;"
        aria-hidden="true" id='modal-input'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myLargeModalLabel">Input News</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="card-body">
                    <?php if(validation_errors()){
                    ?>
                    <?php echo validation_errors('<div class="alert alert-danger alert-dismissible show fade">
                        <div class="alert-body">
                            <button class="close" data-dismiss="alert">
                                <span>×</span>
                            </button>', '</div></div>'); ?>

                    <?php
                     } ?>
                        
                    <div class="form-group">
                        <label>Judul</label>
                        <input type="text" id="judul-input" class="form-control form-control-sm judul" name='judul'
                            value="<?= set_value('judul'); ?>" required>
                    </div>
                    <div class="form-group">
                        <label>Slug</label>
                        <input type="text" id="slug-input" class="form-control form-control-sm" name='slug'
                            value="<?= set_value('slug'); ?>" required>
                    </div>
                    <div class="form-group">
                        <label>Categori</label>
                        <select class="form-select form-control form-control-sm" name='categori'>
                            <option value=''></option>
                            <?php foreach($category as $key => $value){?>
                            <option value='<?= $value['id']?>'
                                <?= (set_value('categori') == $value['id'])?"selected": ""?>><?= $value['name']?>
                            </option>

                            <?php }?>
                        </select>
                    </div>
                    <div class="form-group row">
                        <label class="">Content</label>
                        <div class="col-sm-12 col-md-12">
                            <textarea class="" name='konten' id="content_input"> <?= set_value('konten'); ?></textarea>

                        </div>
                    </div>

                    <div class="form-group">
                        <label>Tags</label>
                        <input type="text" class="form-control form-control-sm" name='tags'
                            value="<?= set_value('tags'); ?>" required>
                    </div>
                    <div class="form-group">
                        <label>Status</label>
                        <select class="form-select form-control form-control-sm" name='status'>
                            <option value='1' <?= (set_value('tags') == 1)?"selected": ""?>>Draft</option>
                            <option value='2' <?= (set_value('tags') == 2)?"selected": ""?>>Pending</option>
                            <option value='3' <?= (set_value('tags') == 3)?"selected": ""?>>Publish</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Publish Date</label>
                        <input type="date" class="form-control form-control-sm" name='publish_at'
                            value="<?= set_value('publish_at'); ?>" required>
                    </div>
                    <div class="form-group">
                        <label>File</label>
                        <input type="file" id="upload-img-input" name='image[]' class=" form-control form-control-sm"
                            accept=".jpg, .png, .jpeg" multiple="multiple"  required>
                        <div class="invalid-feedback" id="validasi-img-input">

                        </div>
                    </div>
                    <div class="form-group">

                        <button class="btn btn-primary mr-1" type="submit">Submit</button>
                    </div>


                </div>
            </div>
        </div>
    </div>

</form>

<!-- Modal View -->
<form method="post" class="needs-validation" action="<?= base_url()?>index.php/News/News/update"
    enctype="multipart/form-data">

    <div class="modal fade bd-example-modal-xl" tabindex="-1" aria-labelledby="myLargeModalLabel" style="display: none;"
        aria-hidden="true" id='modal-view'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myLargeModalLabel">View News</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="padding-20">
                        <ul class="nav nav-tabs" id="myTab2" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab2" data-bs-toggle="tab" href="#about" role="tab"
                                    aria-selected="true">View</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab2" data-bs-toggle="tab" href="#update" role="tab"
                                    aria-selected="false">Edit</a>
                            </li>
                        </ul>
                        <div class="tab-content tab-bordered" id="myTab3Content">
                            <div class="tab-pane fade show active" id="about" role="tabpanel"
                                aria-labelledby="home-tab2">
                                <div class="row" id="div-image">
                                    <!-- <img alt="image" src="<?= base_url() ?>assets/img/users/user-3.png"
                                    style="max-width: 100%; max-height: 100%;" id='view_image'> -->
                                    <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                                        <div class="carousel-inner">
                                            <div id="show-image">
                                                <div class="carousel-item active">
                                                    <img class="d-block w-100"
                                                        src="<?= base_url()?>assets/news/img/blog/img11.png"
                                                        alt="First slide">
                                                </div>
                                                <div class="carousel-item">
                                                    <img class="d-block w-100"
                                                        src="<?= base_url()?>assets/news/img/blog/img07.png"
                                                        alt="Second slide">
                                                </div>
                                                <div class="carousel-item">
                                                    <img class="d-block w-100"
                                                        src="<?= base_url()?>assets/news/img/blog/img08.png"
                                                        alt="Third slide">
                                                </div>
                                            </div>
                                        </div>
                                        <button class="carousel-control-prev" type="button"
                                            data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                            <span class="visually-hidden">Previous</span>
                                        </button>
                                        <button class="carousel-control-next" type="button"
                                            data-bs-target="#carouselExampleControls" data-bs-slide="next">
                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                            <span class="visually-hidden">Next</span>
                                        </button>
                                    </div>
                                </div>
                                <div class="banner-content m-t-20">
                                    <div>

                                        <div class='badge badge-danger fs-12 font-weight-bold mb-3' id="view-tags">
                                        </div>
                                    </div>

                                    <h1 class="mb-2" id='view_judul'><?= $show_news[0]['judul'] ?></h1>
                                    <h1 class="mb-2">

                                    </h1>
                                    <div class="fs-12" style="text-align: justify" id="view_content">
                                        <?= $show_news[0]['konten'] ?>
                                        <!-- <span class="mr-2">Photo </span>10 Minutes ago -->
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane fade" id="update" role="tabpanel" aria-labelledby="profile-tab2">
                                <input type="hidden" class="form-control form-control-sm" name='id_post' id='id_post'
                                    required>
                                <div class="card-header">
                                    <h4>Edit Post</h4>
                                </div>
                                <div class="card-body">
                                    <div class="form-group">
                                        <label>Judul</label>
                                        <input type="text" class="form-control form-control-sm judul" name='judul'
                                            id='judul' required>
                                    </div>

                                    <div class="form-group">
                                        <label>Slug</label>
                                        <input type="text" class="form-control form-control-sm " name='slug'
                                            id='slug-edit' required>
                                    </div>
                                    <div class="form-group">
                                        <label>Categori</label>
                                        <select class="form-select form-control form-control-sm" name='category'
                                            id='category'>
                                            <option value=''></option>
                                            <?php foreach($category as $key => $value){?>
                                            <option value='<?= $value['id']?>'><?= $value['name']?></option>

                                            <?php }?>
                                        </select>
                                    </div>

                                    <div class="form-group row">
                                        <label class="">Content</label>
                                        <div class="col-sm-12 col-md-12">
                                            <textarea class="simple-summernote" name='content'
                                                id='content_edit'></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Tags</label>
                                        <input type="text" class="form-control form-control-sm" name='tags' id='tags'
                                            required>
                                    </div>
                                    <div class="form-group">
                                        <label>Status</label>
                                        <select class="form-select form-control form-control-sm" name='status'
                                            id='status'>
                                            <option value='1'>Draft</option>
                                            <option value='2'>Pending</option>
                                            <option value='3'>Publish</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Publish Date</label>
                                        <input type="date" class="form-control form-control-sm" name='publish_at'
                                            id="publish_at" required>
                                    </div>
                                    <div class="form-group">
                                        <label>File</label>
                                        <input type="file" id="upload-img-edit" name='image[]'
                                            class=" form-control form-control-sm" accept=".jpg, .png, .jpeg" multiple>
                                        <div class="invalid-feedback" id="validasi-img-input">

                                        </div>
                                    </div>

                                    <button class="btn btn-primary" type="submit">Save Changes</button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>


<script>
function previewFile(input) {
    var file = $("input[type=file]").get(0).files[0];

    if (file) {
        var reader = new FileReader();

        reader.onload = function() {
            $("#logo-image-input").attr("src", reader.result);
        }

        reader.readAsDataURL(file);
    }
}



$(document).ready(function() {

    function display(input, image_id) {
        if (input.files && input.files[0]) {
            //cek apakah image
            var allowedTypes = ['image/jpeg', 'image/png', 'image/jpg', 'image/gif'];

            var file = input.files[0]
            var fileType = file.type

            if (!allowedTypes.includes(fileType)) {
                $(input).addClass('is-invalid')
                $(input).next().text('Please choose a valid file (JPEG/JPG/PNG/GIF)')
            } else {

                var reader = new FileReader();
                reader.onload = function(event) {
                    $('#' + image_id + '').attr('src', event.target.result);
                }
                reader.readAsDataURL(input.files[0]);


            }
        }
    }

    $("#upload-img-input").change(function() {
        display(this, 'logo-image-input');
    });

})
</script>
<script>
var tablePost

function getdate(Tanggal) {

    var now = new Date(Tanggal);

    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today = now.getFullYear() + "-" + month + "-" + (day);
    return today
}

function loadTabel(status) {
    tablePost.clear().draw();
    tablePost.ajax.url("<?= base_url('index.php/News/News/get_by_status/') ?>" + status).load()


}

function view(data) {
    id = $(data).data('id')

    $.ajax({
        method: 'POST',
        url: '<?= base_url() ?>index.php/News/News/getDataPost',
        data: {
            id: id
        },
        dataType: "json",
        success: function(response) {
            if (response.success == true) {
                $('#view_judul').text(response.data[0].judul)
                $('#view-tags').text(response.data[0].tags)
                $('#view_content').html(response.data[0].konten)
                index = 0;

                $('#show-image').empty()
                $.each(response.data_image, function(index, value) {
                    if (index == 0) {
                        isactive = 'active'
                    } else {
                        isactive = ''
                    }
                    index = index + 1;
                    $('#show-image').append(
                        "<div class='carousel-item " + isactive +
                        "'><img class='rounded mx-auto d-block mw-100'src='data:image/jpg;charset=utf8;base64," +
                        value.file_image + "' alt='First slide'></div>");
                });
                //isi data untuk di edit
                $('#id_post').val(response.data[0].id)
                $('#judul').val(response.data[0].judul)
                $('#category').val(response.data[0].categori)
                $('#slug-edit').val(response.data[0].slug)
                
                $('#content_edit').summernote({
                    height: 250,
                    minHeight: null,
                    maxHeight: 500,
                    //,airMode: true

                    toolbar: [
                        ["style", ["bold", "italic", "underline", "clear"]],
                        ["font", ["strikethrough"]],
                        // ["para", ["paragraph"]],
                    ],
                    'code': response.data[0].konten
                });

                $('#content_edit').summernote('code', response.data[0].konten);

                // $('.note-editable').html(response.data[0].konten)
                $('#tags').val(response.data[0].tags)
                $('#status').val(response.data[0].status)
                $('#publish_at').val(getdate(response.data[0].publish_at))
                $('#upload-img-edit').val()

                $('#modal-view').modal('toggle')

            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Failed',
                    text: 'Error finding data'
                })
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            Swal.fire({
                icon: 'error',
                title: 'Failed',
                text: 'Error finding data'
            })
        }
    })
}

function del(data) {
    Swal.fire({
        title: 'Warning',
        text: "Are you sure you want to delete this post ?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!',
        focusCancel: true,
        allowOutsideClick: false
    }).then((result) => {
        if (result.isConfirmed) {
            submit_delete = $(data).parent()
            submit_delete.submit();
        }
    })
}
$(document).ready(function() {
    //set summer note
    $('#modal-input').on('shown.bs.modal', function() {
        $('#content_input').summernote({
            height: 250,
            minHeight: null,
            maxHeight: 500,
            //,airMode: true

            toolbar: [
                ["style", ["bold", "italic", "underline", "clear"]],
                ["font", ["strikethrough"]],
                // ["para", ["paragraph"]],
            ],


        });
    })

    $('#modal-view').on('shown.bs.modal', function() {
        // $('#content_edit').summernote({
        //     height: 250,
        //     minHeight: null,
        //     maxHeight: 500,
        //     //,airMode: true

        //     toolbar: [
        //         ["style", ["bold", "italic", "underline", "clear"]],
        //         ["font", ["strikethrough"]],
        // ["para", ["paragraph"]],
        //     ],


        // });
    })


    tablePost = $("#table-post").DataTable({
        orderCellsTop: true,
        fixedHeader: true,
        ordering: true,
        paging: true
    })
    $('#select-all').on('click', function(e) {
        loadTabel(0)
        $('#nav-status a').removeClass('active')
        $(this).addClass('active');

    })
    $('#select-draft').on('click', function(e) {
        loadTabel(1)
        $('#nav-status a').removeClass('active')
        $(this).addClass('active');
    })
    $('#select-pending').on('click', function(e) {
        loadTabel(2)
        $('#nav-status a').removeClass('active')
        $(this).addClass('active');
    })
    $('#select-publish').on('click', function(e) {
        loadTabel(3)
        $('#nav-status a').removeClass('active')
        $(this).addClass('active');
    })


    $('#btn-add').on('click', function(e) {
        $('.form-control').val("");
        $('#modal-input').modal('toggle')

    })


    //generate slug
    $('.judul').keyup(function() {
        var Text = $(this).val();
        var slug = Text.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');
        $('#slug-input').val(slug);
        $('#slug-edit').val(slug);

    });

})
</script>