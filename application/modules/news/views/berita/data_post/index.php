<script>
$(document).ready(function() {
    <?php if($this->session->flashdata('validation_input')){
    ?>
    $('#modal-input').modal('toggle')
    <?php
    }else if($this->session->flashdata('validation_edit')){ ?>

    <?php }?>


    <?php if($this->session->flashdata('err')){

    ?>
    Swal.fire({
        icon: 'error',
        title: 'Failed',
        text: '<?= $_SESSION['err'] ?>'
    })
    <?php
    }?>
    <?php if($this->session->flashdata('msg')){

        ?>
    Swal.fire({
        icon: 'success',
        title: 'Success',
        text: '<?= $_SESSION['msg'] ?>'
    })
    <?php
}?>
})
</script>

<?php $table = $post ?>
<?php $image = $image->result_array() ?>
<div class="row">
    <div class="col-12">
        <div class="card mb-0">
            <div class="card-body">
                <ul class="nav nav-pills" id="nav-status">
                    <li class="nav-item">
                        <a class="nav-link active" id="select-all">All <span
                                class="badge badge-white"><?= $all ?></span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="select-draft">Draft <span
                                class="badge badge-primary"><?= $draft ?></span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="select-pending">Pending <span
                                class="badge badge-primary"><?= $pending ?></span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="select-publish">Publish <span
                                class="badge badge-primary"><?= $publish ?></span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="row mt-4">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4>All Posts</h4>
            </div>
            <div class="card-body">
                <div class="float-left">
                    <div class="selectric-wrapper selectric-form-control selectric-selectric">
                        <button type="button" class="btn btn-primary" id="btn-add">Add Post</button>
                        <div class="selectric-items" tabindex="-1">
                            <div class="selectric-scroll">
                                <ul>
                                    <li data-index="0" class="selected">Action For Selected</li>
                                    <li data-index="1" class="">Move to Draft</li>
                                    <li data-index="2" class="">Move to Pending</li>
                                    <li data-index="3" class="last">Delete Permanently</li>
                                </ul>
                            </div>
                        </div><input class="selectric-input" tabindex="0">
                    </div>
                </div>

                <div class="clearfix mb-3"></div>

                <div class="table-responsive">
                    <table class="table table-striped" id='table-post'>
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Content</th>
                                <th>Category</th>
                                <th>Created At</th>
                                <th>Publish Date</th>
                                <th>Tags</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php foreach ($table as $key => $value){ ?>
                            <tr>
                                <td>
                                    <a>
                                        <span class="d-inline-block ml-1"><?= $value['judul'] ?></span>
                                    </a>
                                    <div class="table-links">
                                        <form action="<?= base_url() ?>index.php/News/delete" method="post"
                                            enctype="multipart/form-data" id="form-input">
                                            <a type="button" data-id="<?= $value['id'] ?>" onclick="view(this)"
                                                class="btn-detail ">Detail</a>
                                            <div class="bullet"></div>
                                            <input type="hidden" value="<?= $value['id'] ?>" name="data">
                                            <a type="submit" class="btn-delete" onclick="del(this)">Trash</a>
                                        </form>
                                    </div>
                                </td>
                                <td>
                                    <p
                                        style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;max-width: 20ch;">
                                        <?= strip_tags($value['konten']);?>
                                    </p>

                                </td>
                                <td>
                                    <?= $value['category_name']?>
                                </td>
                                <td>
                                    <?php $date = date_create($value['create_at']);
                                        echo date_format($date,"d M Y");  ?>
                                </td>
                                <td>
                                    <?php $date = date_create($value['publish_at']);
                                        if(empty($value['publish_at'])){
                                            echo '-';
                                        }else{
                                            echo date_format($date,"d M Y");  

                                        }?>
                                </td>

                                <td>
                                    <?= $value['tags']?>
                                </td>
                                <td>
                                    <?php if($value['status'] == '1') : ?>

                                    <div class="badge badge-danger">Draft</div>
                                    <?php elseif($value['status'] == '2'): ?>

                                    <div class="badge btn-warning">Pending</div>
                                    <?php else: ?>
                                    <div class="badge badge-primary">Published</div>
                                    <?php endif ?>
                                </td>
                            </tr>

                            <?php } ?>


                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>